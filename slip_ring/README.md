When this project first got underway (Sept. 2017), it became obvious that continuous-[pan](https://en.wikipedia.org/wiki/Panning_(camera)) capability would require a through-bore [slip-ring](https://en.wikipedia.org/wiki/Slip_ring) to power the [turret](https://en.wikipedia.org/wiki/Gun_turret).  
  
Commercial [versions](https://www.google.com/search?q=slip+ring+through) are both much too expensive and way over-spec'd for our humble/commodity purposes (< 10 RPM, < $8, ~ 1 inch bore, 3 conductor, 2.5 amp). It seems odd to me that there are no commodity/entry-level product offerings for the broad low-end tinkerer/maker/education/... market, so ...  
  
Taking inspiration from [Carl Pisaturo's work](http://www.carlpisaturo.com/_ElNo_SLIP.html), I settled on a far less elegant (but lighter-resource) methodolgy.  
  
A simple [3-d printed](https://en.wikipedia.org/wiki/3D_printing) [jig](https://en.wikipedia.org/wiki/Jig_(tool)) uses square-profile silicone [O-rings](https://en.wikipedia.org/wiki/O-ring) to hold 3 brass rings (cut/deburred ~0.14" wide from pipe, soldered to wires) annular to a short piece of 3/4" PVC (consumable) for slow-set [epoxy](https://en.wikipedia.org/wiki/Epoxy) casting (petroleum jelly as [mold release agent](https://en.wikipedia.org/wiki/Release_agent)).  

Jig parts :
 * [Slip ring jig - base](https://a360.co/2KCTlaI)
 * [Slip ring jig - cap](https://a360.co/2rUWxa0)
 * [Slip ring jig - shell](https://a360.co/2IyIzBA)  
  
Slip-ring materials :
 * [17 gauge 1.25" dia brass tube](https://www.homedepot.com/p/Eastman-1-1-4-in-x-12-in-Brass-Threaded-Tube-35100/205971636) ([de](https://www.wikihow.com/Remove-Chrome-Plating)-[chromed](https://en.wikipedia.org/wiki/Chrome_plating) w/ [dilute bleach](https://www.lowes.com/pd/Blue-Hawk-Drywall-Mud-Pans/50094776), sanded ([cautions!](https://www.google.com/search?q=chrome+precautions+-browser)))  
   (or eg; https://www.mcmaster.com/#8950k831)
 * [square-profile O-rings](https://www.mcmaster.com/#1182N218)
 * [epoxy](https://www.amazon.com/gp/product/B001NI8MNK)
 * [spring-steel wire](https://www.mcmaster.com/#89085K85) (for slip-ring [brushes](https://en.wikipedia.org/wiki/Brush_(electric)))
 * [copper ferrule](https://www.amazon.com/gp/product/B01BIA5LLS) (mechanical connection (hammered) of brush to wire)  
  
Illustrations :
 * [casting setup](https://drive.google.com/file/d/1BQWS9Qb3ayTcshQhWoXP1I2329IFmYJx/view?usp=sharing)
 * [component assembly](https://drive.google.com/file/d/1Ebx4CrojeMDZ3eX7HesPMi1iNQe6Rojr/view?usp=sharing)  

Useful (but optional-ish ?) tools :
 * [epoxy scale](https://www.amazon.com/gp/product/B0728NV7QG)
 * [de-gassing pump](https://www.amazon.com/gp/product/B0000DE77D)
 * [rubber pad](https://www.lowes.com/pd/Keeney-6-in-Rubber-Washer/1082957)
 * [tubing cutter](https://www.lowes.com/pd/BrassCraft-1-8-in-to-1-1-8-in-Copper-Tube-Cutter/999903321)
 * [ ~ -326 ](https://en.wikipedia.org/wiki/O-ring#Sizes) [jig-banding o-rings](https://www.mcmaster.com/#9557k304) - Ace/Amazon/Grainger/Lowes/HomeDepot/...
 * [pick/hook set](https://www.harborfreight.com/6-piece-pick-set-93514.html) (adjust square-profile o-rings within jig)
