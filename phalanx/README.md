https://www.raspberrypi.com/documentation/microcontrollers/rp2040.html<br>
https://datasheets.raspberrypi.org/rp2040/hardware-design-with-rp2040.pdf
* Chapter 2. Minimal Design Example
   - https://datasheets.raspberrypi.org/rp2040/Minimal-KiCAD.zip
      - yank the headers and mounting holes
         - lay out Minimal on the middle of an rPi0-sized board
      - add some RP2040-driven LEDs, PWM connectors, a speaker, ...
      - front with a cheap ~3M usb cam
