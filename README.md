  ... placed at ... the Garden ... [cherubim and a flaming sword, which turned every way](https://ChurchOfJesusChrist.Org/scriptures/pgp/moses/4.31#p30) to keep the way ...
# guardin
[video@youtube](https://youtu.be/QGgeckfS4LA), [writeup@Hackaday](https://hackaday.com/2018/08/23/guardin-guarding-the-garden-turn-raspberry-pi-into-a-3rd-eye/)  

A continuous-[pan](https://en.wikipedia.org/wiki/Panning_(camera)) turret with a tilt-caged [rPi Zero](https://www.raspberrypi.org/products/raspberry-pi-zero/) + [camera](https://www.raspberrypi.org/products/camera-module-v2/) [trained to recognize](https://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html) and [act vs.](https://en.wikipedia.org/wiki/Pest_control#Cultural_control) eg; [deer](https://en.wikipedia.org/wiki/Deer) (to try to persuade them to munch elsewhere). Aims include an effective commodity-priced alternative to garden fencing.  

Or, just a [maker](https://en.wikipedia.org/wiki/Maker_culture#Philosophical_emphasis)-ish project for exploring [Pi](https://en.wikipedia.org/wiki/Raspberry_Pi) and system development, machine [vision](https://en.wikipedia.org/wiki/Machine_vision) and [learning](https://en.wikipedia.org/wiki/Machine_learning#Learning_classifier_systems), [robotics](https://en.wikipedia.org/wiki/Robotics), [solar power](https://en.wikipedia.org/wiki/Solar_power), ...  

CAD designs for the [through-bore](http://www.carlpisaturo.com/_MaNo_ShBB.html) [slip ring](https://en.wikipedia.org/wiki/Slip_ring) [jig](https://en.wikipedia.org/wiki/Jig_(tool)), [turret](https://en.wikipedia.org/wiki/Gun_turret), and tilt cage are found under [3d/](/3d/)

## pest deterrent objectives
* effective - escalating/combined/randomized multiple modes of deterrence per target [genus](https://en.wikipedia.org/wiki/Genus)
* all modalities acceptable/imperceptible to urban neighbors
* < $100 [DIY/Maker](https://en.wikipedia.org/wiki/Do_it_yourself#Subculture) cost
* 12v solar/battery powered, miserly consumption
   - [step-down](https://en.wikipedia.org/wiki/Buck_converter) to 5v for electronics
   - [step-up](https://en.wikipedia.org/wiki/Boost_converter) to 24v for sprinkler valve

## status
* [3D](/3d/) parts design mostly done
* rough/initial cut of [Python script](https://en.wikipedia.org/wiki/Python_(programming_language)) functional
  - turret pans until target ([eye](https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_eye.xml) for now - via [haar cascade](https://en.wikipedia.org/wiki/Haar-like_feature)) recognized
  - target then tracked (somewhat crudely - may want [PID control](https://en.wikipedia.org/wiki/PID_controller)) ?  

## TODO
* motion-dector [eg; 20-30m doppler](http://www.szhaiwang.cn/en/weibochuanganqi.html) circuit atop tilt cage to trigger system power-up (and reset after encounter)
* [850(/940?)nm](https://elinux.org/Rpi_Camera_Module#Technical_Parameters_.28v.2_board.29) narrow beam [brief/bright](https://www.eetimes.com/document.asp?doc_id=1272233) [near-IR](https://en.wikipedia.org/wiki/Infrared#Regions_within_the_infrared) LED [flash](https://www.raspberrypi.org/forums/viewtopic.php?f=43&t=83484) triggered for night use
  - don't need great images - just good enough for nighttime recognition
* gather sufficient images to [train](https://coding-robin.de/2013/07/22/train-your-own-opencv-haar-classifier.html) a [classifier](https://docs.opencv.org/2.4/doc/user_guide/ug_traincascade.html)
* circuitry to deploy [audio](https://onlinelibrary.wiley.com/doi/abs/10.2193/2006-326), [visual](https://ecosystems.psu.edu/research/projects/deer/news/2015/the-eyes-have-it), and combined aromatic/tactile/light/audio(https://asa.scitation.org/doi/full/10.1121/1.3284546)] (via [sprinkler](https://www.orbitonline.com/products/sprinkler-systems/sprinklers/impact-rotors/plastic-impact-head/12-plastic-impact-head-739) [valve](https://www.orbitonline.com/products/sprinkler-systems/valves/plastic-valves/automatic-jar-top/34-in-npt-jar-top-valve-1825)(+[venturi](https://www.amazon.com/s/ref=nb_sb_noss?field-keywords=3%2F4+venturi+) +UV))] [countermeasures](https://www.merriam-webster.com/dictionary/countermeasure)
  - selectively spray (via venturi) olfactory/taste [offenders](https://www.houselogic.com/organize-maintain/home-maintenance-tips/deer-repellents-for-home/) at/on them and what they're eating (for later deterrence?)
  - [UV](https://en.wikipedia.org/wiki/Ultraviolet) brightener + weak [lasers](https://en.wikipedia.org/wiki/Laser) to mark
  - V/UV [LED](https://en.wikipedia.org/wiki/Light-emitting_diode)/laser to light spray (exaggerate sense of motion)
* protective (weather, insect, ... ) shroud to pull over when tilt-cage is stowed
* [433](https://en.wikipedia.org/wiki/70-centimeter_band)/[900](https://en.wikipedia.org/wiki/33-centimeter_band) [MHz](https://en.wikipedia.org/wiki/ISM_band) phone-home mode ([line-of-sight](https://en.wikipedia.org/wiki/Line-of-sight_propagation)) to report summaries 
* move codebase to C++? (improve current frame processing time of ~4s ?)
* use [OpenSCAD](https://en.wikipedia.org/wiki/OpenSCAD) instead of Fusion/web for CAD images

## Notes
Suggestions welcome, but [working code/designs](http://agilemanifesto.org/) trump all  
([smallest](https://en.wikipedia.org/wiki/Minimalism_(computing)) [pull requests](https://gist.github.com/mikepea/863f63d6e37281e329f8) for the biggest bang)  
https://smallbusinessprogramming.com/optimal-pull-request-size/  
https://www.atlassian.com/blog/git/written-unwritten-guide-pull-requests  
https://hackernoon.com/the-art-of-pull-requests-6f0f099850f9

https://www.almanac.com/pest/deer  
https://www.epicwilderness.com/infographic-deer-senses/  
https://www.grandviewoutdoors.com/big-game-hunting/whitetail-deer/scientific-facts-about-how-deer-see-and-hear/  

https://en.wikipedia.org/wiki/Deer  
https://en.wikipedia.org/wiki/Mule_deer  
https://en.wikipedia.org/wiki/White-tailed_deer  
http://www.dnrec.delaware.gov/fw/Hunting/Documents/Nonlethal%20Deer%20Damage%20Abatement%20Techniques%20-%20Deterrents.pdf
