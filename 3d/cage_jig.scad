// © 2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/cage_jig.scad
// Jig to hold tilt_cage mouth open until electronics are in and frameClip()'d.
// Rear of cage should already secured by frameClip

include <params.scad> ;

module endClip() {
  legW = pi0crnR + slop + tCgRailThick ;
  translate([-(tCgW + 2*slop + 2*tCgRailThick)/2, 0, 0]) {
   cube([tCgRailThick, 2*tCgRailThick, legW + 4*tCgRailThick]) ;
   translate([tCgRailThick, tCgRailThick, legW + 2*tCgRailThick + slop])
     sphere(d=2*tCgRailThick) ;
  }
}

module frameJig() {
  rotate([90,0,0]) {
    translate([0, tCgRailThick, tCgRailThick/2]) {
      cube([tCgW + 2*(tCgRailThick + slop),
            2*tCgRailThick, tCgRailThick], center=true) ;
      translate([0,0, -antiZ + tCgRailThick/2])
        rotate([0, 90, 0])
        cylinder(pi0L - 2*(pi0crnR + slop), d=2*tCgRailThick, center=true) ;
    }
    endClip() ;
    mirror([1, 0,0])
      endClip() ;
  }
}

frameJig() ;

translate([0, 1, 0])
  rotate([0,0,180])
  frameJig() ;
