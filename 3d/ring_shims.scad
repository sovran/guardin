// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/ring_shims.scad
//
// non-conducting shims separating slip-ring rotor's conducting rings

use <ring_stack.scad>

ringStack() ;
