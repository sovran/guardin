// © 2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/sprinkler.scad

// meshlab Sprinkler.obj # => .stl
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Importing_Geometry#import

include <params.scad> ;
include <sg90_params.scad> ;

module import_scan() {
  translate([-5.1,18.3,51])
    rotate([91,-1,-30.7])
    import("./Sprinkler.stl") ;
}

module scan_top() {
  translate([0,0, 35])
    import_scan() ;
}

c2dia = 17.1 ;
c2ht = 9.6 ;

c3dia = 14 ;
c3ht = 9 ;

c4dia = 5.5 ;
c4ht = 5.3 ;

module h2oWay(grow=0) {
  // color("blue", .1)
  translate([h2oTubeCtrX, h2oTubeCtrY, 0]) {
    cylinder(collarOverChin, d=collarDia + grow) ;
    cylinder(collarOverChin + c2ht, d=c2dia + grow) ;
    cylinder(collarOverChin + c2ht + c3ht, d=c3dia + grow) ;
    cylinder(collarOverChin + c2ht + c3ht + c4ht, d=c4dia + grow) ;
  }
}

frameThick = 3.3 ;
frameWd = 6.8 ;
frame30len = 28.2 ;
frame30rot = -31 ;
frame30z = 12.3 ;
frameTopLen = 33 ;
frameTopThick = 2.75 ;
frameTopOD = 18 ;
framePinOD = 4.85 ;
pinCylOD = 10.1 ;

// ribThick = 3.1 ;
ribThick = (pinCylOD - framePinOD) / 2 + .3;
frontVertRibThick = 6.85 ;

module sprinkler(grow=0) {  // grow only along what will be X - TODO cos() ?
  difference() {
    union() {
      h2oWay(grow) ;
      translate([h2oTubeCtrX, 0, frame30z]) {
        rotate([0, frame30rot, 0])
          translate([0, 0, -grow/2])
          cube([frame30len, frameWd, frameThick + grow]) ;
        translate([21.2 - grow/2, 0, 14.2])
          rotate([0, -7.5, 0])
          cube([frameThick + grow, frameWd, 31.8]) ;
      }

      // upper vane pivot hole
      translate([0, 0, 56]) {
        cube([h2oTubeCtrX, frameTopOD, frameTopThick]) ;
        cube([frameTopLen, frameWd, frameTopThick]) ;
        translate([h2oTubeCtrX, h2oTubeCtrY, 0]) {
          cylinder(frameTopThick, d = frameTopOD);
          translate([0, 0, -3.8])
            cylinder(3.5, d = pinCylOD) ;
        }
      }

      translate([0, frameTopOD/2 + framePinOD / 2, 52.2])
        cube([13.2, ribThick - .3, 5]) ; // front upper horizontal rib
      translate([12.3, frameWd - ribThick, 52.2])
        cube([20, ribThick, 5]) ; // rear upper horizontal rib
      rotate([0, -8, 0]) // rear vertical rib
        translate([30.8, frameWd - ribThick, 20.6])
        cube([8, ribThick, 31]) ;
      translate([0, frameWd - ribThick, 19.5])
        cube([28, ribThick, 8]) ; // base rib

      rotate([0, 5, 0]) // front vertical rib
      translate([-8.5, frameTopOD - frontVertRibThick, 22])
        cube([5.5, frontVertRibThick, 35]) ;

      // brow
      translate([-25, 0, 49])
        rotate([0, -15, 0])
        cube([26.8, frameTopOD, frameTopThick + .2]) ;

      // lower vane pivot hole
      translate([0, 0, 24.8]) {
        cube([h2oTubeCtrX, frameTopOD, frameTopThick]) ;
        translate([h2oTubeCtrX, h2oTubeCtrY, 0])
          cylinder(frameTopThick, d = frameTopOD);
      }

      // nozzle base/under
      translate([-5.5, 0, 16.5]) {
        rotate([0,25,0])
          cube([frameTopThick, frameTopOD, 13.5]) ;
        rotate([0,26,0])
          cube([11, frameTopOD, frameTopThick]) ;
      }

      // screw side
      translate([0, -1, 0])
        cube([screwMntDeep, 2.5, chinHt]) ;
    }
    // thru/vane_pivot hole
    translate([h2oTubeCtrX, h2oTubeCtrY, -50])
      cylinder(110, d = framePinOD) ;
  }
}

module sprinklerMask(grow=0) {
  sprinkler(grow) ;
  translate([-antiZ, 0, -antiZ])
  cube([h2oTubeCtrX + 2*antiZ, frameWd,
        bodyHt + gearsHt + turretWallThick + 2*antiZ]) ;
  translate([0, -antiZ, 0])
    rotate([-90,0,0])
    linear_extrude(frameWd + 2*antiZ)
    polygon([[h2oTubeCtrX, -frame30z],
        [h2oTubeCtrX, -(bodyHt + gearsHt + turretWallThick + antiZ)],
        // TODO - trig instead of #
        [37, -(bodyHt + gearsHt + turretWallThick + antiZ)]]) ;
}

// sprinklerMask() ;

sprinkler() ;
%import_scan() ;
