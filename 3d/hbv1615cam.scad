// © 2021 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/hbv1615.scad

// shim to stand a 1.3MP USB HBV-1615 camera off from a pi0 prototyping board :
// https://www.hbvcamera.com/1-3megapixel-usb-cameras/1.3megapixel-usb-digital-camera-module.html
// ~ $3.50 @ https://www.aliexpress.com/item/32898768162.html

include <params.scad>
use <pi0shim.scad>

wire4H = 3.25 ;	// 4-wire socket height
wire4L = 7.33 ;	// socket L along pcb edge; male plug .5mm > socket width (7.5)
wire4ctrO = 15.35 - wire4L/2 ;	// offset of socket's ctr from nearest end
wire4deep = 4.5 ;

lensDia = 13.9 ;
lensH = 10.85 ;

pcbT = 1 ;	// pcb thickness
pcbW = 28.25 ;	// pcb length : along pi0L
pcbH = 28.11 ;	// pcb width : across pi0W

mountDia = 2 ;
padDia = 4 ;

railW = .75 ;

// notches/indents on camera board
notchDia = mountDia; // 2.5 ;
notchCtrOffset = 2 ; // 3.15 ;

module tang () {
  translate([-pcbW/2 + notchCtrOffset, -pcbH/2 + notchCtrOffset, 0]) { // -X,-Y quadrant
    translate([0, -((pi0W - pcbH)/2 + notchCtrOffset)/2, wire4H/2])
      cube([padDia, (pi0W - pcbH)/2 + notchCtrOffset, wire4H + antiZ], center=true) ;
    // standoff - support pcb
    cylinder(wire4H + antiZ, d = padDia) ;
    // fit mount posts
    cylinder(wire4H + pcbT, d = notchDia - slop) ;
  }
}

module attachmentsQ() {
  tang() ;
}

module attachments() {
  mirror_copy([0, 1, 0]) // mirror/copy half-shim across y-axis
    mirror_copy() // mirror/copy quarter-shim across (default) x-axis
      attachmentsQ() ;
}

module plug(add=0) {
  translate([pcbW/2 - wire4ctrO, // - wire4L/2,
             pcbH/2 - wire4deep/2,
             -wire4H/2 + antiZ])
    cube([wire4L + add, wire4deep, wire4H + 2*add], center=true) ;
}

module mount() {
  translate([pcbW/2 - notchCtrOffset,
             pcbH/2 - notchCtrOffset,
             -antiZ])
    cylinder(pcbT + 2*antiZ , d=mountDia + slop) ;
}

module mounts() {
  mirror_copy()
    mirror_copy([0, 1, 0])
      mount() ;
}

module cam() {
  difference() {
    union() {
      translate([0,0, pcbT/2])
        cube([pcbW, pcbH, pcbT], center=true) ;
      translate([0,0, antiZ])
        cylinder(pcbT + lensH, d=lensDia) ;
      #plug() ;
    }
    mounts() ;
  }
}

module retainer() {
  translate([-(pcbW - 2*padDia)/2, -pi0W/2, wire4H + 1.25 ])
    difference() {
      cube([pcbW - 2*padDia, (pi0W - pcbH)/2 + notchCtrOffset/2, 1]) ;
      rotate([40, 0,0])
        translate([-antiZ,0, -1 -.5])
          cube([pcbW - 2*padDia + 2*antiZ, (pi0W - pcbH)/2 + notchCtrOffset/2 + .5, 1]) ;
    }
}

module hbv1615shim() {
  difference() {
    union() {
      translate([0,0, wire4H])
        %cam() ;
      attachments () ;
      translate([0,0, wire4H])
      pi0shim(railW, 2.25, 0, wire4H) ;
    }
    // cut-through for 4-wire plug
    translate([0, wire4H, -2*antiZ + wire4H])
      plug(.5 + slop) ;	// wire plug is .5 bigger than socket
  }
  mirror_copy([0, 1, 0]) // mirror/copy half-shim across y-axis
    retainer() ;
}

// printing orientation
rotate([180,0,0])
  hbv1615shim () ;
