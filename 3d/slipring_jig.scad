// © 2019-2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/slipring_jig.scad

// jig for dressing/smoothing stator/rotor pieces

include <params.scad> ;
use <hex_piece.scad> ;

p150Thick = .65 ;	// 44 ;	// e.g. p150 grit sandpaper thickness
handleOD = ringOD - 2 * p150Thick;

slipHt = inch ;	// approx ht of slip ring
handleHt = 50 ;	// carpe jig

// https://www.hillmangroup.com/us/en/Fastening-Solutions/Specialty/Washers/Fender-Washers/Neoprene-Fender-Washer-%283-16%22-x-1-1-4%22-x-1-16%22%29/p/2861
// available @Ace/HD/Lowes ... an option to press sandpaper against stator bottom
// 2861-A	rubber fender washer	1.25 OD x 3/16 ID x 1/16 in.
fenderID =  inch * 3 / 16 ;	// #8 screw - .19"

echo("hexPcHt = ", hexPcHt) ;
rotorJigHt = hexPcHt + hexSlide - hexThreadHt ;
echo("rotorJigHt = ", rotorJigHt) ;

module jig() {
  difference () { // cut slots for sandpaper
    union () {
      cylinder(slipHt + handleHt, d = handleOD);	// stator section
      translate([0,0, slipHt + handleHt - hexThreadHt])	// skip threads
        hexPiece (-.25) ;	// rotor section
    }
    translate([0, 0, slipHt / 2]) {	// stator section
      // slot for 1 thickness of sandpaper
      cube([handleOD, p150Thick, slipHt], center = true);
      // partial slot on 1 side for 2 thickness of sandpaper
      translate([handleOD * .2, 0,0])
        cube([handleOD * .6, 2 * p150Thick, slipHt], center = true);
    }
    translate([0, 0, slipHt + handleHt + rotorJigHt / 2]) {	// rotor sectoin
      cube([handleOD, p150Thick, rotorJigHt], center = true);	// 1 thickness
      translate([handleOD * .2, 0,0])
        cube([handleOD * .6, 2 * p150Thick, rotorJigHt], center = true); // 2 thick
    }
    translate([0,0, -antiZ])
      cylinder(slipHt, d = fenderID - .25) ;	// screw hole rubber fender washer
  }
}

jig() ;
