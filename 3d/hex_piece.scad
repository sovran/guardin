// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/hex_piece.scad

include <params.scad> ;

// https://www.orbitonline.com/products/sprinkler-systems/sprinklers/impact-rotors/plastic-impact-head/12-plastic-impact-head-739/55018
// https://en.wikipedia.org/wiki/Impact_sprinkler#Design_and_operation
// for the Orbit sprinkler, the attachment nut piece = threads, hex, waist, collar

module hexPiece(growD = 0) { // grow/shrink XY diameters by +/- growD
  // position hexBase @z=0 (threads below XY plane)
  translate([0,0, -hexThreadHt])
  difference () {
    union () {
      cylinder(hexPcHt + hexSlide, d = hexCollarOD + growD) ;
      cylinder(hexThreadHt, d = hexThreadOD + growD) ;	// male NPT threads
      translate([0,0,hexThreadHt]) {
        cylinder(hexHt, d = hexOD + growD) ;	// cylinder circumscribing
        %cylinder(hexHt, d = hexOD + growD, $fn=6) ;	// hex attachment nut
        translate([0,0,hexHt]) {
          cylinder(hexWaistHt, d = hexWaistOD + growD) ;
        }
      }
    }
    translate([0,0, -antiZ])
      // sleeve for inner H2O tube
      %cylinder(hexPcHt + hexSlide + antiZ * 2, d = hexPcID) ;
  }
}

hexPiece() ;
