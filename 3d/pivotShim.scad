// © 2021 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/pivotShim.scad
//
// A shim for an axle to run through and a servo wire to connect to

include <params.scad>
include <sg90_params.scad>
use <pi0shim.scad>

pivotId = springDia ;
pivotOD = 5.5 ;

shimThick = 2.5 ;
shimWide = springDia ;

clearSD = pi0SD_overhang + .5 ; // stay clear of the SD card
clearCam = 1.75 ;

jut = 5.1 ; // out over the top of turret pivot to match servo arm

// 1/32" music wire as servo linkage (push-rod), e.g.
// https://www.hobbylobby.com/Crafts-Hobbies/Hobbies-Collecting/Tools-Blades/Music-Wire---0.032%22/p/25260
wireD = .81 ; // 1/32" = .032" measured

module axleHub(thick=shimWide) {
  // regardless of thickness, align to inside of shim's short leg
  translate([pi0L/2 - shimWide/2 + (thick - shimWide)/2, 0,0]) // thick/2, 0, 0])
    rotate([0, 90, 0])
      cylinder(thick, d=pivotOD, center=true);
}

module armXY(armT) { // build arm in first octant
  armX = 2 ; // extra length beyond pushrod hole

  difference() {
    intersection() {
      cylinder(armT, r = armC2cL + armX) ;
      translate([0, 0, -antiZ])
        cube([armC2cL + armX, armC2cL + armX, armT + 2*antiZ]) ;
    }

    // holes for servo pushwire attachment
    for (i = [0:0])
    for (rot = [i%2 ? 9 : 5 : 8 : 90]) {
      translate([(armC2cL - i*armH2h) * cos(rot), (armC2cL - i*armH2h) * sin(rot), - antiZ])
        cylinder(armT + 2 * antiZ, d = wireD + 3.7 * slop) ;
    }
  }
}
% armXY(1.5) ;

module pivotArm() { // place the pivot arm
  translate([pi0L/2 + clearSD, 0,0])
    rotate([90, 0, 0])
      rotate([0, -90, 0])
        armXY(2);
}

standoff = (pivotOD - shimThick)/2;
module pivotShim () {
  difference() {
    union() {
      translate([0, 0, -shimThick/2])
        pi0shim(shimWide, shimThick, (pivotOD - shimThick)/2, 1.5, shimWide - .55) ;

      // hubs
      mirror_copy([1, 0, 0]) // mirror/copy axle hub across x-normal (yz) plane
        axleHub();

      // plus more to clear SD card overhang
      translate([shimWide - antiZ, 0,0])
        axleHub(clearSD) ;

      // and another to protect the PiCam connector overhang
      translate([-shimWide + antiZ, 0,0])
        mirror([1,0,0])
          axleHub(clearCam);

      pivotArm() ; // for the servo wire to push against
    }

    // axle hole through all
    translate([-pi0L/2 - clearCam - antiZ, 0,0])
      rotate([0, 90, 0])
        cylinder(pi0L + clearSD + clearCam + 2*antiZ, d = springDia + 2.5*slop);
  }
}

pivotShim() ;


module axleShim(t=springDia) {
  difference () {
    cylinder(t, d=pivotOD) ;
    translate([0,0, -antiZ]) // hole for spring wire cage axle
      cylinder(2*antiZ + t, d = springDia + 2.25*slop) ;
  }
}

axleShim(3) ;
