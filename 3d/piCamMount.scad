// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/piCamMount.scad
//
// A shim to stand a piCamera off from a pi0 prototyping board :
// https://makerspot.com/prototyping-board-for-raspberry-pi-zero/

include <params.scad>
use <pi0shim.scad>

//https://www.raspberrypi.org/documentation/hardware/camera/
//https://www.raspberrypi.org/documentation/hardware/camera/mechanical/rpi_MECH_Camera2_2p1.pdf
cornerRadius = 2 ;
holeDia = 2.2 ;
holePadDia = 4.5 ;
holeXYoffset = 2 ;	// corner offset of mounting holes
holesXmid = 14.5 ;	// width offset of mid-lens mounting holes
camBrdL = 23.862 ;
camBrdW = 25 ;
camBrdT = 1 ;

edgeInset = (pi0W - camBrdW) / 2 ;
// holeCtrOffset = edgeInset + holeDia / 2 ;
holeCtrOffset = edgeInset + holeXYoffset ;
// shimWd = 2 ;
shimThick = 1.5 ;
standoff = 4 ;	// z height to loft base of cam board ~> cable socket depth

module tang () {
  translate([0, -pi0W / 2, 0]) {
    translate([-holePadDia / 2, 0, 0]) {
      cube([holePadDia, holeCtrOffset, shimThick]) ;
      translate([holePadDia / 2, holeCtrOffset, 0]) {
        cylinder(standoff - shimThick, d = holePadDia) ;
        translate([0, 0, -antiZ + standoff - shimThick])
          cylinder(camBrdT + 2, d = holeDia - .1) ;
      }
    }
  }
}

module tangs () {
  tang() ;
  mirror([0,1,0]) tang() ;
  translate([-(holesXmid - holeXYoffset), 0, 0]) {
    tang() ;
    mirror([0,1,0]) tang() ;
  }
}

module piCamShim () {
  pi0shim(shimThick, shimThick) ;
  tangs () ;
  // TODO - ghost of piCam
}

piCamShim () ;
