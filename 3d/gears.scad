// © 2019-2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/gears.scad

include <params.scad> ;
include <sg90_params.scad> ;
use <thru_holes.scad> ;
use <MCAD/involute_gears.scad> ;

module statorGear() {
  // translate([0,0,statorGearHt])
  rotate(180, [1,0,0])
    difference () {
      gear(
        number_of_teeth=39,
        gear_thickness = statorGearHt,
        rim_thickness=statorGearHt,
        hub_thickness=0,
        clearance = 0.2,
        pressure_angle=30,
        bore_diameter= ringID + .4 ,
        circular_pitch=200
      );
    thruHoles(h=statorGearHt, bite=false) ;
  }
}

// servoGearHt = 5.5 ;
servoGearHt = statorGearHt ;

module servoGear() {
  translate([0,0, -servoGearHt])
    difference () {
      gear(
        pressure_angle=30,
        number_of_teeth=9,
        rim_thickness=servoGearHt,
        gear_thickness = servoGearHt,
        hub_thickness = 0,
        bore_diameter = 0,
        circular_pitch= 200
      );
    // sg90 splined shaft
    translate([0,0, servoGearHt - 2.42 -antiZ])
      cylinder(servoGearHt + 2*antiZ, d = .1885 * inch ) ;

    // sg90 shaft screw thread OD
    cylinder(servoGearHt - 2.42 + antiZ, d = splineScrewOD + .5) ;

    // sg90 shaft screw head OD
    translate([0,0, -antiZ])
      cylinder(servoGearHt - 2.42 - 1.1, d = splineScrewHeadOD + .5) ;
  }
}

// z=0 is the top-of-gear/bottom-of-sprinkler-chin - gears mesh just below
statorGear() ;

translate([32.5, 0,0])	// offset servo gear along x to hint at meshing
  servoGear() ;
