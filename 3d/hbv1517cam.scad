// © 2019-2021 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/hbv1517cam.scad

// shim to stand a 1.3MP USB HBV-1517 camera off from a pi0 prototyping board :
// https://www.hbvcamera.com/1-3megapixel-usb-cameras/1.3megapixel-usb-digital-camera-module.html
// ~$4 @ https://www.aliexpress.com/i/32899714404.html

include <params.scad>
use <pi0shim.scad>

wire4H = 3.5 ;	// 4-wire socket height
wire4L = 7.25 ;	// socket L along pcb edge; male plug .5mm > socket width (7.5)
wire4ctrO = 9.6 + wire4L/2 ;	// offset of socket's ctr from nearest end
wire4deep = 4.5 ;

lensDia = 14 ;
lensH = 11 ;

pcbT = .85 ;	// pcb thickness
pcbW = 30.15 ;	// pcb length along notched (long) sides
pcbH = 25 ;	// pcb width : short side

// notches/indents on camera board
notchDia = 2.5 ;
notchCtrOffset = 3.15 ;

module tang () {
  shelfW = notchDia + 1.5 ;
  translate([-pcbW/2 + notchCtrOffset, -pcbH/2, 0]) { // -X,-Y quadrant
    translate([0, -notchDia/2, wire4H/2]) //(pi0W - pcbH)/2, wire4H/2])
      cube([shelfW, (pi0W - pcbH)/2, wire4H], center=true) ;
    // standoff - support pcb
    cylinder(wire4H, d = shelfW) ;
    // fit notch
    cylinder(wire4H + pcbT, d = notchDia + slop) ;
    // retaining frustrum
    translate([0,0, wire4H + pcbT - antiZ])
      cylinder(pcbT, d1 = notchDia + slop, d2 = shelfW) ; // notchDia + 1 ?
  }
}

module clip(xtra=0) {
  // cap protrusion to hook into frame
  rotate([90,0,0])
  translate([-pi0L/2, 0, pi0W/2])
  translate([pi0crnR - springDia,
             wire4H + 1.5
             - tCgCapDeep + springDia/2,
             -antiZ]) {
    translate([0,0, -pi0crnR + .8])
      cylinder(2*antiZ + pi0crnR - .8, r=tCgCapDeep-springDia/2) ;
    cylinder(2*antiZ + tCgRailThick - (springDia + xtra)/2, d=springDia+xtra) ;
    translate([0,0, tCgRailThick - (springDia + xtra)/2])
      sphere(d=springDia+xtra) ;
  }
}

module attachmentsQ() {
  tang() ;
  clip() ;
}

module attachments() {
  mirror_copy([0, 1, 0]) // mirror/copy half-shim across y-axis
    mirror_copy() // mirror/copy quarter-shim across (default) x-axis
      attachmentsQ() ;
}

module notch() {
  translate([-pcbW/2 + notchCtrOffset, -pcbH/2, -antiZ])
    cylinder(2*antiZ + pcbT, d=notchDia);
}

module notches() {
  mirror_copy([0, 1, 0]) // mirror/copy half-shim across y-axis
    mirror_copy() // mirror/copy quarter-shim across (default) x-axis
      notch() ;
}

module plug(add=0) {
  translate([pcbW/2 - wire4ctrO, // - wire4L/2,
             pcbH/2 - wire4deep/2,
             -wire4H/2 + antiZ])
    cube([wire4L + add, wire4deep, wire4H + 2*add], center=true) ;
}

module cam() {
  difference() {
    union() {
      translate([0,0, pcbT/2])
        cube([pcbW, pcbH, pcbT], center=true) ;
      translate([0,0, antiZ])
        cylinder(pcbT + lensH, d=lensDia) ;
      #plug() ;
    }
    notches() ;
  }
}

module hbv1517shim() {
  difference() {
    union() {
      translate([0,0, wire4H])
        %cam() ;
      attachments () ;
      pi0shimBlank(1, wire4H + 1.5) ;
    }
    // cut-through for 4-wire plug
    translate([0, wire4H, -2*antiZ + wire4H])
      plug(.5 + slop) ;	// wire plug is .5 bigger than socket
    pi0shimMntHoles(wire4H + 1.5) ;
  }
  translate([0, pi0W/2 - springDia/2, wire4H + 1.5 - springDia/2])
    rotate([0, 90, 0])
      cylinder(pi0L - 2*pi0crnR, d=springDia, center=true) ;
}

%hbv1517shim () ;
// printing orientation
rotate([180,0,0])
  hbv1517shim () ;
