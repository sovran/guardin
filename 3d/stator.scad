// © 2019-2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/stator.scad

include <params.scad> ;
use <thru_holes.scad> ;
use <MCAD/regular_shapes.scad> ;
use <hex_piece.scad> ;

layerLen = 38.15 ;	// TODO - parameterize from ringOD
layerWidth =  ringOD + 3 ;
brushTrackHt = springDia + 2.65*slop ; // overage for 1/16" spring steel wire rotor brush
brushTrackWidth = 4;
brushTrackTranX = (layerWidth - brushTrackWidth)/2 ;

oldID = 26.5 ;
statorOD = ringOD + 12.5 ;	// + mounting holes width * 2
trackWd = (statorOD - oldID + antiZ) / 2 ;

module indent() {
  /*
  // difference() w/ cyl wedge ?
  rotate_extrude(convexity = 4)
    translate([statorOD/2 - brushTrackHt/2, 0, 0]) {
      circle(d = brushTrackHt) ;
      translate([0, -brushTrackHt/2])
        square([brushTrackHt/2 + antiZ, brushTrackHt]) ;
    }
  */
  xtra = 1.5 ;
  translate([statorOD/2 - brushTrackHt/2 - xtra, 0,0])
    rotate([90, 0,0])
      cylinder(statorOD, d=brushTrackHt, center=true) ;
  translate([statorOD/2 - brushTrackHt/2 - xtra,  -statorOD/2, -brushTrackHt/2])
    cube([brushTrackHt/2 + xtra + antiZ, statorOD, brushTrackHt]) ;
}
// %indent() ;

module trackPair() {
  // center brushes to ring
  translate([0, statorOD/2 - trackWd/2, 0])
    cube([statorOD, trackWd, brushTrackHt], center=true);
  translate([0, -statorOD/2 + trackWd/2, 0])
    cube([statorOD, trackWd, brushTrackHt], center=true);
  indent() ;
}
// %trackPair() ;

module brushTracks() {
  for (ringN = [0 : 1 : nRings - 1])
    translate([0,0, ringN * 2 * ringHtDflt])
      rotate(ringN * 60)
         trackPair();
}

baseHt = 2;  // underlying rotor cut-out
raceHt = 0 ;  // possible film/washer bearing/race under rotor ? PTFE ?
twixtHt = raceHt + 2*slop ; // space 'twixt top of base and rotor bottom
rotorHt = ringStackHt ; // scratch
echo("subGearHt = ", rotorHt) ; // scratch
statorHt = baseHt + twixtHt + ringStackHt ;

module base() {
  translate([0,0,-baseHt]) {
    difference() {
      cylinder(baseHt, d=statorOD);
      // fit over hex attachment nut
      translate([0,0, -antiZ])
        // PLA+ prints ... different ?
        cylinder(baseHt + antiZ * 2, d=hexOD + .4, $fn=6);
    }
  }
}

module weepHoles() {	// to drain if/any water
  for (rot = [0 : 60 : 300]) {
    rotate(rot)
      translate([ringOD / 2 - 1.75, 0, -antiZ])
        cylinder(baseHt + antiZ * 2, d1=5, d2=4) ;
  }
}

module stator() {
  difference () {
    translate([0,0, -baseHt - twixtHt]) // z=0 at rotor bottom
      cylinder(statorHt, d = statorOD) ;
    // rotor
    translate([0,0, -twixtHt])
      cylinder(twixtHt + ringStackHt + antiZ, d = ringOD + .65) ;
    // waist 
    translate([0,0, -antiZ - baseHt - twixtHt])
     cylinder(twixtHt + ringStackHt + 2*antiZ, d = hexWaistOD + 3*slop) ;
    // brush tracks
    translate([0,0, 1.5 * ringHtDflt])
      brushTracks() ;
    // gear-mounting screwholes
    rotate(-30)
      // thruHoles(h=statorHt, zXlat = -baseHt - twixtHt, sink=false) ;
      thruHoles(h=statorHt, zXlat = -baseHt - twixtHt, sink=false, shorten1=brushTrackHt) ;
  }
}
stator() ;

// height of rotor beneath the bottom of stator gear
subGearHt = ringsHt + (nRings + 1)*spacerHt ; // includes base

// height of rotor beneath the collor (bottom of stator gear)
subCollarHt = subGearHt + statorGearHt ;

translate([0,0, subCollarHt - hex2chin + 1])
%hexPiece() ;


// connect stator to hex
hextHt = 8 ;
hextThick = 1.25 ;
leverHt = 1;

// connect stator to hex nut
module stator2hex() {
  difference() {
    union() {
      scale([(hexOD + 2*hextThick) / statorOD, 1,1])
        cylinder(leverHt, d = statorOD) ;
      cylinder(hextHt, d=hexOD + 2*hextThick, $fn=6);
    }
    translate([0,0, -antiZ])
      cylinder(hextHt + 2*antiZ, d=hexOD + .5, $fn=6);
    rotate([0, 180, 0])
      rotate(30)
        thruHoles(h=leverHt, zXlat=-leverHt, sink=false) ;
  }
}

translate([-statorOD -1, 0, -baseHt - twixtHt])
  stator2hex() ;

// suggest positioning of stator2hex
translate([0,0, -baseHt - twixtHt -antiZ])
  rotate([0, 180, 0])
    %stator2hex() ;
