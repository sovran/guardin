// © 2019,2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/rotor.scad

// rotor holding rings spins within stator holding brushes

include <params.scad> ;
use <funcs.scad> ;
use <gears.scad> ;
use <hex_piece.scad> ;
use <ring_stack.scad> ;

ringsHt = vsum(ringHts);

// height of rotor beneath the bottom of stator gear
subGearHt = ringsHt + (nRings + 1)*spacerHt ; // includes base

// height of rotor beneath the collor (bottom of stator gear)
subCollarHt = subGearHt + statorGearHt ;

coreHt = subCollarHt + topCollarXtraHt ;
widthAboveTopCollar = 16.95 ;	// truncates collar cylindricality

// e.g. https://www.homedepot.com/p/Cerrowire-65-ft-20-2-Solid-Bell-Wire-206-0101BA3/202304706
// (available by the foot)
// or, e.g. https://www.amazon.com/gp/product/B01K4RPF1C/ref=ppx_yo_dt_b_search_asin_title
wireOD = 1.75 ;	// -ish mm (3/64" - 1/16+")
channelWd = wireOD + .25 ;	// remember the solder joint

// hexPcHt = 37.83 ;	// measured total
hexAttchHt = hexThreadHt + hexHt + hexWaistHt + hexCollarHt + hexSlide;
echo("subCollarHt, hex2chin, subCollarHt - hex2chin = ",
      subCollarHt, hex2chin, subCollarHt - hex2chin) ;

module channel (ht) {
  // cylinder along z, extended along x
  cylinder(ht, d = channelWd) ;
  translate([0, -channelWd/2, 0])
    cube([4, channelWd, ht]) ;
}

module wireChannel(deg = 0) {
  ht = coreHt - spacerHt + antiZ ;
  translate([cos(deg) * (ringID/2 -channelWd/2),
              sin(deg) * (ringID/2 -channelWd/2),
              spacerHt])
  // channel carved in outer rotor - cylinder along z, extended along x
    rotate(deg, [0,0,1]) {
      channel(ht) ;
      // tilt wires away from gearID - avoid contact/abrasion
      translate([0,0, subGearHt - spacerHt -spacerHt/4])
        rotate(-30, [0,1,0])
          channel(9);
    }
}

module wireChannels(nC) {
  for (c = [1 : nC])
    wireChannel(-c * 90/(nC+1)) ;
}

module rotorCore() {
  union() {
    difference () {
      union () {
        cylinder( spacerHt, d = ringOD ) ;  // base - supports ring/spacer stack
        cylinder( coreHt, d = ringID - slop) ;  // rings/spacers slide/stack over
      } ;
      // cutout for sub-chin collar
      translate([0,0, coreHt-collarHt])
        cylinder( collarHt + antiZ, d = topCollarDia + 0.25 ) ;
      // rotor rides on the attachment nut piece
      // hexBase is hex2chin below subCollar/gearTop
      translate([0,0, subCollarHt - hex2chin + 1]) {
        hexPiece(3*slop) ;
        %hexPiece(3*slop) ; // display transparent hexPiece ghost in preview mode
      }

      translate([0,0, spacerHt])
        %ringStack() ;
      translate([0,0, subGearHt])
        translate([0,0, statorGearHt])  // pull up from below z=0
        %statorGear() ;

      // main sprinkler chin cutaway
      translate([-topCollarDia/2, 7.7, coreHt - topCollarXtraHt])
        color("red")
        cube ([25.4, 10, inch * 3 / 32 + antiZ]) ;
      // nip the point catching the collar truncation
      translate([0,0, coreHt - topCollarXtraHt])
        color("green")
        cube ([widthAboveTopCollar/2, 10, topCollarXtraHt + antiZ]) ;
      // turret cutaways
      translate([-inch+topCollarDia/2 - 4.85,
                 -inch*3/4+7.7-inch*5/32 - 9.4,
                 coreHt - topCollarXtraHt])
        color("blue")
        cube ([inch/2 + .5, inch*3/4 + 4, topCollarXtraHt + antiZ]) ;

      wireChannels(len(ringHts)) ; // each ring is a conductor with a wire attached
    }
  }
}

rotorCore() ;
