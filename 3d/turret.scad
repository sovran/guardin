// © 2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/turret.scad

// meshlab Sprinkler.obj # => .stl
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Importing_Geometry#import

include <params.scad> ;
include <sg90_params.scad> ;
use <sprinkler.scad> ;
use <branding.scad> ;

threadCyXoff = 5.85 ;
threadCyLen = 11.4 ;
threadCyOD = 8 ;

// cavityLen = 66.15 ;
cavityLen = 48 ; // length of actual servo cavity
frontThick = 15 ;

screwCtrZ = 6.2 ;
screwCtrY = 3.6 ;

springDeep = 1.5 ;
servoSpringDia = inch * 5 / 16 ;

turretLen = turretWallThick + cavityLen + springDeep + turretWallThick ;

module turret(brandingInset=0) {
  //color("blue", .2)
    //%sprinkler() ;
  difference() {
    union() {
      // turret bunker
      translate([threadCyXoff + threadCyLen - turretWallThick,
                 -(bodyWdMax + turretWallThick), 0])
        cube([turretLen,
              turretWallThick + bodyWdMax + turretWallThick,
              bodyHt + gearsHt + turretWallThick]) ;
      // screw standoff
      translate([threadCyXoff + threadCyLen + antiZ, -screwCtrY, screwCtrZ])
        rotate([0, -90, 0])
          cylinder(threadCyLen, d = threadCyOD) ;
      // backside hook
      translate([33.7, -antiZ, bodyHt + gearsHt + turretWallThick - 6])
        cube([7, 10, 6]) ;
      //branding/PR
      if (brandingInset < 0) // outset - add
        translate([0, -(bodyWdMax + turretWallThick), 0])
          branding(L = turretLen,
                   H = bodyHt + gearsHt + turretWallThick,
                   inset = brandingInset) ;
    }
    //branding/PR
    if (brandingInset > 0) // inset - subtractive
      translate([0, -(bodyWdMax + turretWallThick), 0])
        branding(L = turretLen,
                 H = bodyHt + gearsHt + turretWallThick,
                 inset = brandingInset) ;

    // #sprinkler() ;

    // servo (main) cavity
    // translate([frontThick, -bodyWdMax, -antiZ])
    translate([threadCyXoff + threadCyLen, -bodyWdMax, -antiZ])
      // cube([cavityLen - frontThick - springDeep, bodyWdMax + antiZ,
      cube([cavityLen, bodyWdMax,
           bodyHt + gearsHt]) ;
    // servo-spring inset
    // translate([cavityLen - antiZ, -bodyWdMax / 2, 16])
    // rotate([0, -90, 0])
      // #cylinder(springDeep, d = servoSpringDia + .5) ;
    translate([threadCyXoff + threadCyLen + cavityLen + springDeep - antiZ,
               -bodyWdMax / 2, 16])
        rotate([0, -90, 0])
          cylinder(springDeep, d = servoSpringDia + .5) ;

    // servo wire block cut-thru
    // translate([threadCyXoff + threadCyLen + cavityLen - antiZ,
    translate([threadCyXoff + threadCyLen -turretWallThick - antiZ,
               // -bodyWdMax + (bodyWdMax - wireBlkWd)/ 2,
               -bodyWdMax/2 - wiresWd/2 + antiZ,
               bodyHt + gearsHt - bodyCapHt - wireBlkHt])
      // #cube([turretWallThick + springDeep + antiZ * 2, wireBlkWd, wireBlkHt]) ;
      cube([turretWallThick + springDeep + antiZ * 2, bodyWdMax/2 + wiresWd/2, wiresHt + 1]) ;

    // screw mount
    translate([-antiZ, -(screwMntWd - screwMntHt/2), chinHt / 2]) {
      translate([0,0, -screwMntHt / 2 - .1])
        cube([screwMntDeep + .1, screwMntWd - screwMntHt/2 + antiZ,
             screwMntHt + .1]) ;
      rotate([0, 90, 0])
        cylinder(screwMntDeep + .2, d = screwMntHt + .2) ;
    }

    //translate([-antiZ, -1.1, 0])
      //cube([screwMntDeep + .1, 1.1 + antiZ, chinHt + .1]) ;

    // translate([-antiZ, -4.5, -antiZ])
      // ube([screwMntDeep + .1, 4.5 + antiZ, chinHt/2 - screwMntHt/2 + .1]) ;

    // screw hole
    translate([-antiZ + threadCyXoff, -screwCtrY, screwCtrZ])
      rotate([0, 90, 0])
      // cylinder(frontThick + antiZ * 2, d = screw8MjDia - .25) ; // screw hole
      cylinder(threadCyLen + antiZ * 2, d = screw8MjDia - .25) ; // screw hole

    // sprinkler collar
    translate([h2oTubeCtrX, h2oTubeCtrY, -antiZ])
      cylinder(collarOverChin + .1, d = collarDia + .25) ;

    // holes for spring-wire to retain/push servo
    translate([33.5, -(bodyWdMax + turretWallThick + antiZ), 14.5])
      rotate([-90, 0, 0])
        cylinder(turretWallThick*2 + bodyWdMax + 2*antiZ, d = springDia) ;
    translate([-antiZ + threadCyXoff + threadCyLen - turretWallThick,
                -bodyWdMax/2, 16])
      rotate([0, 90, 0])
        // cylinder(turretLen + 2*antiZ, d = springDia) ;
        cylinder(turretWallThick + 2*antiZ, d = springDia * 1.1) ;

     sprinklerMask(.3) ;
  }
  // hold-up complementing back-side hook
  translate([threadCyXoff + threadCyLen - turretWallThick,
             -antiZ, bodyHt + gearsHt + turretWallThick - 1])
    cube([4, turretWallThick, 1]) ;
}

module turretFace(brandingInset) {
  translate([-turretLen/2, -antiZ + bodyWdMax, 0])
              // bodyHt + gearsHt + turretWallThick]) ;
  difference() {
    turret(brandingInset) ;
    translate([-antiZ, -bodyWdMax, -antiZ])
        cube([turretLen + 2*antiZ,
              bodyWdMax + turretWallThick + 7,
              bodyHt + gearsHt + turretWallThick + 2*antiZ]) ;
  }
}

// experiment - inscribe branding on the billboard (turret side)
/*
turretFace(.6) ;  // recess/inset/engrave
rotate([0, 0, 180])
  turretFace(-.6) ; // raised/outset/emboss
*/

%turret() ;  // ghost showing normal orientation
rotate([0, 180, 0]) // orient for printing
  turret() ;
