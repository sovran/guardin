// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/M2_5.scad

// M2.5 rod (can be used in stead of 2.5mm screws?

$fn = 100 ;

rotate(90,[0,1,0])
  cylinder(80, d=2.5, center=true) ;
