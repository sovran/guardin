// © 2019-2021 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/params.scad

// shared parameters for various bits - rings, brushes, sprinkler, boards, ...

include <funcs.scad> ;

$fn = 100 ;
inch = 25.4 ;	// mm

// https://en.wikipedia.org/wiki/Pin_header
// 2.54 mm (0.100 in) with 0.64 mm (0.025 in) square pins[6]
pinHdrPitch = 2.54 ; // 0.1" header pin sep

turretWallThick = 2 ;

// rotor slip rings (rotating conductors) - cut from brass tubing
// https://www.mcmaster.com/brass-tubing
// https://www.mcmaster.com/8950K831 Brass Tubing 1-1/4" OD, 0.032" Wall
/*
ringOD = 1.25 * inch ;	// 31.75 mm	// 1.5" - 38.1 mm
ringID = 1.186 * inch;	// 1.5"OD => 1.436"ID
ringWallThick = .032 * inch;	// .813 mm	// 1/16" - 1.626 mm
*/

// https://www.mcmaster.com/8950K771/
ringOD = 1.125 * inch ;	// 1-1/8" +/- .004*inch	// => 28.575 mm +/- .1 mm
ringID = .995 * inch;	// ~1" ID
ringWallThick = .065 * inch;	// >~1/16"  - 1.65 mm // (ringOD - ringID)/2

//  - 1' tube => dozens(?) of 1/16" rings, depending on cutting methods' waste
ringHtDflt = inch / 16 ; // spacers same Ht as this
spacerHt = ringHtDflt ;	// print to same as machined ring

ringHts = [ringHtDflt, ringHtDflt] ; // assume consistent
ringsHt = vsum(ringHts) ;
nRings = len(ringHts) ;
ringStackHt = ringsHt + (nRings + 1)*spacerHt ; // + rotor base is spacerHt

// https://www.mcmaster.com/91771a115 1" 4-40 screw
screw4HdHt = .067 * inch ;	// 1.7 mm
screw4HdDia = .212 * inch ;	// 5.38 mm
screw4MjDia = .112 * inch ;	// 2.84 mm

screw8HdHt = 3 ;
screw8HdDia = 8 ;
screw8MjDia = 4.15 ;	//.16 * inch ;	// ~ 5/32"

// through-hole for connecting stator to e.g. gear
// screws to ~overmatch OD, len ~ 7/16, 5/8, 15/16 "
thruOD = screw4MjDia ;	// prints smaller ? as ~?mm => #4
thruOffset = ringOD /2 * 1.2 ; // 19;	// distance from z-axis of through-hole

// stator brushes
// https://www.mcmaster.com/89085K85	304 Stainless Spring-Steel Wire
springDia = inch / 16 ;

// https://www.orbitonline.com/products/sprinkler-systems/sprinklers/impact-rotors/plastic-impact-head/12-plastic-impact-head-739/55018
// hex attachment nut : https://en.wikipedia.org/wiki/Impact_sprinkler
hexThreadHt = 10.4 ;
hexThreadOD = 13/16 * inch ;	// below hex

hexHt = 8.56 ;	// ~ 3.08 + 2.3 + 3.08
hexOD = 24.0 ;
hexFlats = 20.85 ;

hexWaistHt = 8.46 ;	// 7.54 + 1.05 ;	// atop hex
hexWaistOD = 20.01 ;	// measured

hexCollarHt = 10.36 ;	// full - 10.28 - 10.37
hexCollarOD = 15.55 ;	// 39/64"	// atop hexWaist

hexSlide = 8.46 - 6.63 ; // 1.83 mm	// slides along tube this distance
hexID = 67/128 * inch ;	// slips over 1/2" OD , 5/16 ID H2O tube

// hexPcHt = 37.78 ;	// measured total
hexPcHt = hexThreadHt + hexHt + hexWaistHt + hexCollarHt ;  // + hexSlide ?
hexPcID = 67/128 * inch ;	// slips over 1/2" OD , 5/16 ID H2O tube
hex2chin = 29 ;	// .32 ;  // distance from hex bottom to chin (gear top)

collarDia = 20.1 ;
collarHt = 6.9 ;
collarOverChin = 3.6 ;
collarSubChin = 2.3 ;

// h20 tube center offset from turret(0,0)
h2oTubeCtrX = 13.05;	// 12.8 ;
h2oTubeCtrY = 8.88;	// 9.18 ;

chinHt = 13.13 ;  // screwMnt (but not screw hole) is centered in chin
screwMntHt = 8 ;
screwMntWd = 9.3 ;
screwMntDeep = 5.5 ;

topCollarDia = 20.1 ;
topCollarXtraHt = inch * 3 / 32 ;	// ht of portion !complete cyl
botCollarHt = 8.75 ;
botCollarDia = 20 ;

chinZ = 0 ;	// chin of sprinkler head
tubeOD = 15.55 ;	// ~5/8 (39/64) in

statorGearHt = 4.55 ; // current gear height/thickness

// https://www.raspberrypi.org/documentation/hardware/raspberrypi/mechanical/README.md
// https://www.raspberrypi.org/documentation/hardware/raspberrypi/mechanical/rpi_MECH_Zero_1p3.pdf
pi0L = 65 ;
pi0W = 30 ;
pi0crnR = 3 ;
pi0mntHoleD = 2.75 ;
pi0mntPadD = 6 ;
pi0mntCtrXY = 3.5 ;
pi0SD_overhang = 3 ;	// SD card extends beyond end of pi0

// https://learn.adafruit.com/16-channel-pwm-servo-driver/downloads
// Adafruit PCA9685 16-Channel PWM Servo Driver	// lots of clones
pwmBrdWd = 2.45 * inch ;	// 62.23 mm
pwmBrdDeep = (.88 + .13) * inch ;	// 25.65 mm
pwmBrdCornerRadius = .13 * inch ;	// 3.3 mm	// == XYoffset of hole

// reduce Z-fighting ghosting
// https://en.wikipedia.org/wiki/Z-fighting
// https://www.youtube.com/watch?v=CjckWVwd2ek
antiZ = .001 ;

slop = .15 ;	// generic play

// tilt_cage params needed for tilt_frame
tCgCapDeep = 2.2 ;	// depth (thickness) of tilt cage end caps
tCgRailThick = 1.5 ;	// thickness of tilt cage rail
tCgRailUsable = 28 ;	// depth of usable tilt cage rail (exclude cap thicks)
tCgAxleHt = norm([tCgRailUsable/2 + tCgCapDeep, pi0W/2 + slop + tCgRailThick]) + 5.5 ;
tCgW = pi0L + 2*(slop + tCgRailThick) ;
