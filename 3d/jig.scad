// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/ring_jig.scad

include <params.scad> ;
use <MCAD/regular_shapes.scad> ;

// jig for marking/deburring rotor rings
// top side is for marking
//  - insert squared/dressed end of tube and mark off 1/8 ring
// bottom for deburring/working freshly cut ring
//  - insert cut ring with side needing deburring, etc up
//  - optional o-ring for convenience/workability, e.g. @local ACE;
jigHt = 1.5 * inch ;
jigID = inch ;
jigOD = 1.75 * inch ;

// https://www.hillmangroup.com/us/en/Fastening-Solutions/Specialty/O-Rings/Nitrile-O-Rings/Nitrile-O-Ring-%281-1-4%22-x-1-1-8%22-x-1-16%22-size%29/p/56018
markORingThick = 0 ;	// inch / 16 ;	// 0 for no o-ring
workORingThick = inch / 16 ;	// 0 for no o-ring

markSlotHt = ringHtDflt + markORingThick ;
markOrigin = jigHt-markSlotHt ;

workHt = ringHtDflt * 0.35 ;
workSlotHt = workHt + workORingThick ;

slotOD = ringOD + .2 ;
slotID = ringID - .2 ;

module jig() {
  difference () { // cut a work area through middle of
    union () { // add in mark/work ID cyls
      difference () { // blank cyl - ODs
        cylinder(jigHt, d=jigOD) ;
        translate([0, 0, markOrigin]) cylinder(markSlotHt, d = slotOD) ;
        cylinder(workSlotHt, d=ringOD) ;
      }
      translate([0, 0, markOrigin]) cylinder(markSlotHt, d = slotID) ;
      cylinder(workSlotHt, d = slotID) ;
    }
    cylinder(jigHt, d=jigID) ;
  }
}

module o_ring () {
  // cylinder_tube( // (height, radius, wall, center = false)
  cylinder_tube( inch / 16, slotID / 2, (slotOD - slotID) / 2 - .1) ;
}

jig() ;
translate([jigOD + .5, 0, 0])
  o_ring() ;
