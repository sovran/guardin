// © 2019,2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/ring_stack.scad
//
// non-conducting spacers 'twixt rotor's conducting slip-rings

include <params.scad>

xtraOD = .1 ;
xtraID = -.1 ;
xtraHt = -.1 ;

module spacer(h = spacerHt) {
  difference () {
    cylinder( h + xtraHt, d = ringOD + xtraOD) ;	// outer
    translate([0,0, -antiZ])
      cylinder( h  + xtraHt+ antiZ * 2, d = ringID + xtraOD) ;	// inner
  }
}

module ring(h = ringHtDflt) { spacer(h) ; }

// TODO - parameterize on len(ringHts)
module ringStack() {
// for a 3-ring rotor, need 2 full- and 1 half-height spacers
%ring(ringHts[0]) ;
  translate([0,0,ringHts[0]]) {
    #spacer() ;
    translate([0,0,spacerHt]) {
      %ring(ringHts[1]) ;
      translate([0,0,ringHts[1]]) {
/* TODO - recurse ?
        #spacer() ;
        translate([0,0,spacerHt]) {
          %ring(ringHts[2]) ;
          translate([0,0,ringHts[2]])
*/
            #spacer(spacerHt) ;
//      }
      }
    }
  }
}

ringStack() ;
