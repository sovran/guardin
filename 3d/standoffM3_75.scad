// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/standoffM3_75.scad

// 3.75mm hollow-ish stand-off accepts M2.5 rod

$fn = 100 ;
len = 80 ;
OD = 3.756 ; // insert M2.5
ID = 2.6 ; // insert M2.5

rotate(90,[0,1,0]) {
  difference () {
    cylinder(len, d=OD, center=true) ;
    cylinder(len, d=ID, center=true) ;
  }
}
