// © 2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/ring_slicer.scad

// jig for cutting off 1/16" high brass slip-rings
// 2 hacksaw blades connected by springs, e.g.;
//  - https://www.lowes.com/pd/LENOX-10-in-Bi-Metal-Medium-Cut-Hacksaw-Blade-Set/1003172356
//  - https://www.hillmangroup.com/us/en/Home-Solutions/Home-Accessories/Springs/Extension-Springs/Hillman-Extension-Spring/p/540470
//  - #8-32 x 3/8" machine screws + lock nuts
// https://wedo.hillmangroup.com/us/en/Fastening-Solutions/Fasteners/Screws/Machine-Screws/Zinc-Pan-Head-Phillips-Machine-Screw/p/92118
// https://wedo.hillmangroup.com/us/en/Fastening-Solutions/Fasteners/Nuts/Stop-%26-Lock-Nuts/Zinc-Plated-Nylon-Insert-Stop-Nut-USS-Coarse/p/180138

// Nylon Specialty Washer (1.25" Outer Dia. x 0.250" Inner Dia. x 0.062" Thick)
//  Hillman Item #59567
// https://wedo.hillmangroup.com/us/en/Fastening-Solutions/Specialty/Washers/Nylon-Washers/Nylon-Specialty-Washer-%281-25%22-Outer-Dia-x-0-250%22-Inner-Dia-x-0-062%22-Thick%29/p/59567

include <params.scad> ;

// we want 1/16" (1.588 mm) slip rings - tweak diff 'twixt code and reality
ringHt = inch/16 + .3 ; //2*slop ; // thickness of rings to slice from tube

module tube(l = 1 * inch, oDia = ringOD + .004 * inch, iDia = ringID) {
  // https://www.mcmaster.com/8950K831/
  // 1-1/4" OD (+/- .004"), 1.186" ID, 0.032" Wall Thickness
  difference() {
    cylinder(l, d = oDia);
    translate([0,0, -antiZ])
      cylinder(l + 2 * antiZ, d = iDia);
  }
}

hackWide = inch/2 ;  // hacksaw blade width
hackThick = .75 ; // ~ inch * 3 / 32 ;  // hacksaw blade thickness (wavy teeth)

tubeSlop = 4*slop + .05 ;
tubeNslop = ringOD + tubeSlop ;
jigOD = tubeNslop + 2 * hackWide ;

nylonWasherThick = 0; // 0.0635 * inch ; // measured - nominal = .062
// Nylon Specialty Washer (1.25" Outer Dia. x 0.250" Inner Dia. x 0.062" Thick)

baseThick = 3 ; // washer flush to top of
deckThick = 4 ; // above hacksaw slot
tubeSleeveHt = 4 ; // outer tube above ellipse deck
subRing = baseThick + nylonWasherThick + ringHt ;
supraRing = hackThick + deckThick ;
hTot = baseThick + subRing + supraRing ;
tubeRcv = hTot - baseThick ;

module blank(od = jigOD, h = hTot, scaleBy = 1.5) {
  difference() {
    scale([scaleBy, 1,1])
      cylinder(h, d = od);

  }
}

difference() {
  union() {
    blank(h = hTot - tubeSleeveHt) ;
    cylinder(h = hTot, d = tubeNslop + 5);
  }
  // holes for thru-pins
  #translate([jigOD * 1.5 * .35, 0, -antiZ])
    cylinder(2*antiZ + hTot, d = 2) ; // ?
  #translate([-jigOD * 1.5 * .35, 0, -antiZ])
    cylinder(2*antiZ + hTot, d = 2) ; // ?

  // tube sleeve
  translate([0,0, baseThick])
    cylinder(antiZ + hTot - baseThick, d = tubeNslop) ; // ?
  // track for hacksaw blade
  translate([0,0, baseThick + nylonWasherThick + ringHt])
    difference() {
      blank(jigOD * (1 + antiZ), hackThick) ;
      blank(inch, hackThick, 3.42) ;
    }
  // peek @ bottom
  translate([0,0, -antiZ])
    cylinder(2 * antiZ + baseThick, d = 0.95 * inch);

}
// nylon washer
translate([0,0, -antiZ + baseThick])
  %tube(2*antiZ + nylonWasherThick, ringOD, 0.8 * ringOD) ;
// ring slice
translate([0,0, -antiZ + baseThick + nylonWasherThick])
  %tube(ringHt) ;
// tube end
translate([0,0, baseThick + nylonWasherThick + hackThick])
  %tube() ;

translate([0, -65, 0])
difference() {
  blank(h = hTot - tubeSleeveHt) ;
  // holes for thru-pins
  #translate([jigOD * 1.5 * .35, 0, -antiZ])
    cylinder(2*antiZ + hTot, d = 2) ; // ?
  #translate([-jigOD * 1.5 * .35, 0, -antiZ])
    cylinder(2*antiZ + hTot, d = 2) ; // ?
  // tube sleeve
  translate([0,0, -antiZ])
    cylinder(antiZ*2 + hTot, d = tubeNslop) ; // ?
}

// Nylon Specialty Washer (1.25" Outer Dia. x 0.250" Inner Dia. x 0.062" Thick)
//  Hillman Item #59567
// https://wedo.hillmangroup.com/us/en/Fastening-Solutions/Specialty/Washers/Nylon-Washers/Nylon-Specialty-Washer-%281-25%22-Outer-Dia-x-0-250%22-Inner-Dia-x-0-062%22-Thick%29/p/59567

// 1/16" high slip-ring spacer - calibrate jig
module calibration_disc() {
  // should print/measure to 1/16" (1.59 mm)
  // and slip exactly 'twixt washer and slotted hacksaw blades
  cylinder(ringHt, d = ringOD + .004 * inch) ;
}

baseHt = 7 ;
wallThick = 1.5 ;

// jig to hold ring for abrading freshly-cut rough side to thickness
module dressing_jig() {
  difference() {
    union() {
      cylinder(baseHt, d = ringOD + 1) ;
      translate([0,0, -antiZ + baseHt])
        cylinder(ringHt, d = ringID - slop) ;
    }
    translate([0, 0, -antiZ])
      cylinder(2*antiZ + baseHt + ringHt, d = ringID - 2*wallThick) ;
  }
}

bladeOD = inch * 5 /32 + slop ;
holderC2C = hackWide + ringID ;
holderLen = holderC2C + 25 ;
holderWd = bladeOD * 3 ;
holderThick = 3 * hackThick ;

module bladeCutout(cRot = false) {
  // pin/screw hole
  cylinder(holderThick + 2*antiZ, d = bladeOD) ;

  // slot for blade + some rotation
  translate([0,0, holderThick/4 - antiZ]) {
    cube([hackWide + slop, holderWd + 5, holderThick/2], center=true);
    rotate([0,0, cRot ? 15 : -15])
      cube([hackWide + slop, holderWd + 5, holderThick/2], center=true);
  }
}

module holdBlades() {
  difference() {
    translate([0,0, holderThick/2])
      cube([holderLen, holderWd, holderThick], center=true) ;
    translate([holderC2C/2, 0, -antiZ])
      bladeCutout() ;
    translate([-holderC2C/2, 0, -antiZ])
      bladeCutout(true) ;
  }
}
// translate([-holderC2C/2, 90, 0])
translate([0, 75, 0])
rotate([180, 0,0])
holdBlades() ;

translate([-20, 50, 0])
  calibration_disc() ;

translate([20, 50, 0])
  dressing_jig() ;
