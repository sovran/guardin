// © 2019,2021 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/sg90_params.scad

// the tilt-servo adjusts the angle of the tilt cage (atop the turret)
// the continuous-rotation pan-servo spins the turret vs. the stator gear
// both are based on an sg90 form factor

// Mechanical drawings - will seek to generalize (+ vs. reality) :
// http://www.towerpro.com.tw/product/sg90-7/	// SG90 digital
// http://www.towerpro.com.tw/product/sg90-analog/
// http://www.towerpro.com.tw/product/sg90-360-degree-continuous-rotation-servo/
// http://www.arduinoos.com/2016/02/sg90-servo-part-1/

$fn = 100 ;

bodyHt = 22.7 ;
bodyWdMax = 12.6 ;
bodyWdMin = 12.2 ;	// CR
bodyLen = 22.8 ;	// CR: 22.7
bodyCapHt = 5.26 ;	// included in bodyHt
bodyMidHt = 10 ;	// height of middle section of body

wingOffsetZ = 4.5 ;	// from flat gear deck
wingThick = 2.5 ;
wingSpan = 4.7 ;
wingHoleOff = 2.3 ;
wingScrewOD = 2 ;
wingScrewLen = 8 ;
wingSlotWd = 1.3 ;

wiresHt = 1.2 ;
wiresWd = 3.6 ;

gearsHt = 4 ;
gearsLen = 14.6 ;
smGearOD = 5.5 ;
// lgGearOD = 11.66 ;

// splined shaft
splinesHt = 3.2 ;
splinesOD = 4.84 ;
splinesID = 4.69 ;
splineScrewOD = 2.05 ;
splineScrewHeadOD = 3.21 ;

wireBlkHt = 3 ;
wireBlkWd = 8 ;

armHoles = 6 ;
armC2cL = 14.3 ; // ctr-ctr distance from servo axle to outermost linkage hole
armH2h = 2 ; // distance between arm holes for linkage/pushwire/pushrod
