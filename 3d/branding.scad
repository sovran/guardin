// © 2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/branding.scad

// brand GuardIn.Org + QR onto turret

include <params.scad> ;
include <sg90_params.scad> ;
include <guardinQR.scad> ; // via https://github.com/l0b0/qr2scad

letterDeep = .3 ;

module xy2dText(L, H) {
  translate([3, 7]) {
    // GuardIn.Org
    translate([0, 10])
      text("GuardIn", font="Bitstream Vera Sans Mono:style=Bold", size=6) ;
    translate([1.5, 0])
      text(" .Org", font="Bitstream Vera Sans Mono:style=Bold", size=6) ;
   }
}

module xy3dQR(L, H, T) {
  // https://en.wikipedia.org/wiki/QR_code
  // python3-qrcode -
  //  => qr http://GaurdIn.Org --error-correction=H > guardinQR.png
  // qr2scad < gaurdinQR.png > gaurdinQR.scad # <include>

  qrScale = (H-3) / qr_code_size ; // vs. H less 1.5 edge on top/bottom
  // echo("qrScale = ", qrScale) ;
  translate([L - H/2 -1.5, H/2, 0])
    scale([qrScale, qrScale, T])
      qr_code() ;
}
// %xy3dQR(L=70.65, H=29.7, T=.4) ;
// qr_code() ;

module xy3D(L, H, T) {
  linear_extrude(T)
    xy2dText(L, H) ;
  xy3dQR(L, H, T) ;
}
// xy3D(L=70.65, H=29.7, T=.4) ;

// JUNK - rm  mask()+ //  TODO - make relative to L,H
qr_mm = 24.5 ;
qrX = 44 ;
qrZ = 3 ;

module mask(L, H) {
  translate([0, -7, 0])
  rotate([90, 0, 0]) // into XZ plane
  difference() {
    // shell around turret side
    translate([L/2, H/2, 0]) {
      shellDeep = 5 ;
      translate([0,0, -(5 - letterDeep - slop)/2])
      difference() {
        cube([L + 4*slop + letterDeep,
              H + 4*slop + letterDeep,
              shellDeep],
              center=true) ;
        translate([0,0, -(letterDeep - slop)])
          cube([L + 4*slop, H + 4*slop, shellDeep], center=true) ;
      }
    }

    translate([0,0, -(letterDeep + slop + 2*antiZ)/2]) {
      // kid0()
      linear_extrude(letterDeep + slop + 2*antiZ)
        offset(r=slop) // TODO - fattest 1 children(0?)
        xy2D(L, H) ;

    // rm loose QR bits
    translate([qrX + 1.65, qrZ + 1.25, 0])
      cube([qr_mm - 2.8, qr_mm - 2.8, letterDeep + slop + 2*antiZ]) ;
    }
  }
}

module kid0() {  // TODO - only 1 child from linear_extrude() ?
  difference() {
    children() ;
    children([1:$children-1]) ;
  }
}

// given a billboard of given L,H, paint GuardIn branding on it
module branding(L, H, inset) { // inset<0 means outset
  echo("qr_code_size = ", qr_code_size) ;
  echo("L = ", L, "  H = ", H, " inset = ", inset) ;

  // ~against y=0 plane (front@inset>0, rear@inset<0 (outset))
  translate([0, (inset>=0 ? inset - antiZ : antiZ), 0])
    rotate([90, 0, 0]) // tilt XY branding into XZ orientation @y=0
      xy3D(L, H, abs(inset)) ;
  // mask(L, H) ;
}
branding(L=70.65, H=29.7, inset=.4) ;
// mask(70.65, 29.7) ;
