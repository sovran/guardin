// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/sg90.scad

include <sg90_params.scad> ;

alpha = .3 ;	// ~=opacity
antiZ = .001 ;

module wing() {
  translate([-bodyLen/2, 0,0])	// from bodyLen center, offset to x=0
  translate([-wingSpan, 0, wingOffsetZ])  // offset wing by x/y -span/zOffset
  color("Blue", .5)
  difference() {
    cube([wingSpan, bodyWdMax, wingThick]) ;
    // cut slot
    translate([-antiZ, (bodyWdMax - wingSlotWd) / 2, -antiZ])
      cube([wingSpan - wingHoleOff + antiZ * 2, wingSlotWd,
            wingThick + antiZ * 2]) ;
    // drill screw thru-hole
    translate([wingSpan - wingHoleOff, bodyWdMax / 2, -antiZ])
      cylinder(wingThick + antiZ * 2, d = wingScrewOD) ;
  }
}

module cap() {
  difference () {
    color("Blue", alpha)
      cube([bodyLen - .01, bodyWdMax - .01, bodyCapHt]) ;
    translate([bodyLen - 1, (bodyWdMax - wiresWd) / 2, 0])
      cube([1, wiresWd, wiresHt]) ;	// servo wire bundle opening
  } 
}

module caplessBody() {
  color("Blue", alpha)
    cube([bodyLen, bodyWdMax, bodyHt - bodyCapHt]) ;
}

module gears() { // centered on axis of largeGear/spline/shaft
  color("White")
    translate([0,0, -splinesHt])	// spline/shaft underneath z=0
      cylinder(splinesHt, d = splinesOD);
  color("Blue", alpha) {
    cylinder(gearsHt, d = bodyWdMax) ;	// largeGear dia ==? bodyWdMax
    // translate([-(gearsLen - bodyWdMax/2 - smGearOD/2), 0,0])
    translate([-bodyWdMax/2, 0,0])
      cylinder(gearsHt, d = smGearOD) ;
  }
}

module sg90() {
  translate([wingSpan, 0,0]) { // exterior wing corner above origin
    translate([bodyLen - bodyWdMax / 2, bodyWdMax / 2, 0])
      gears() ;
    translate([0,0, gearsHt]) {
      translate([0,0, bodyHt - bodyCapHt])
        cap() ;
      caplessBody() ;
      translate([bodyLen / 2, 0,0]) {
        wing() ;
        mirror([1,0,0])
          wing() ;
      }
    }
  }
}

// %sg90() ;
