// © 2021 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/rPicoShim.scad

// To use a Raspberry Pi Pico μC
// https://www.raspberrypi.org/documentation/pico/getting-started/
// in lieu of e.g. an https://www.adafruit.com/product/815
// PCA9685 I2C 16-Channel 12-bit PWM/Servo Driver
// Servo connectors are usually a 1x3 female block of 0.1" connectors.
//
// So if the Pico is SMT mounted on a carrier, its PWM pins will be
// higher than the carrier's Gnd/Pwr pins by the 1mm thickness of the Pico.
// Ergo, this can shim up the carrier board pin so the servo connection
// isn't skeewampus.

// https://components101.com/sites/default/files/2021-01/Raspberry-Pi-Pico-Microcontroller-Datasheet.pdf
picoThick = 1 ; 

$fn = 100 ;
antiZ = .001 ;

// https://en.wikipedia.org/wiki/Pin_header
// 2.54 mm (0.100 in) with 0.64 mm (0.025 in) square pins[6]
//   or precision machined 0.50 mm (0.020 in) round pins[7]
pinW = .64 ;
pinDia = pinW * sqrt(2) ; // .905
padW = 2.54 ;

xL = 20 ; // 2x20 pin header
yW = 2 ;

module pinShim() {
  translate([padW/2, padW/2, picoThick/2])
    difference() {
      cube([padW + antiZ, padW + antiZ, picoThick], center=true) ;
      cylinder(picoThick + 2*antiZ, d=pinDia + .75, center=true) ;
    }
}

difference() {
  for(x=[0:xL-1])
    for(y=[0:yW-1])
      translate([x*padW, y*padW, 0])
         pinShim() ;
  translate([-antiZ, -antiZ, -antiZ])
    cube([xL * padW + 2*antiZ, padW/8, picoThick + 2*antiZ]) ;
}
