// © 2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/tilt_servo_shim.scad

// include <params.scad> ;
include <sg90_params.scad> ;

antiZ = .001 ;
sideWd = 1 ;

module side() {
  cube([sideWd, 15, 2.5]) ;
}

cube([bodyWdMax, 15, 1]) ;

translate([-sideWd + antiZ, 0,0])
  side() ;

translate([bodyWdMax - antiZ, 0,0])
  side() ;
