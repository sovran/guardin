// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/M1_6.scad

// 1/16" dia rod ~ 1.588 mm

$fn = 100 ;
inch = 25.4 ;

rotate(90,[0,1,0])
  cylinder(80, d = inch / 16, center=true) ;
