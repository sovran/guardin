// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/tilt_cage.scad

// This cage houses pi0-sized stuff, and tilts through ~160 degrees.
// At the beginning of an encounter, it 'un-parks' and looks forward.
// The system then pans looking for a target, and this cage tilts as
// necessary to center an identified target in the field-of-view (FoV).
// At the end of an encounter, this cage 'parks' with the aperture
//  nestled down into the tilt_frame base, (to protect camera, etc).

// TODO - tCgCapDeep => 2, round ? ; fix/mirror servoPush ; min params ; cleanup
// servoPush difference()s *after* everything vertical struts

include <params.scad> ;

use <MCAD/regular_shapes.scad> ;
use <pi0shim.scad> ;

sg90srvoArm = 11 ; // 13.3 ; // 15 max // gear ctr to outermost hole of sg90 servo arm : --~1.7

// spokeThick = 2 ;
spokeThick = tCgRailThick ;
spokeWd = 2.5 ;

module axleShim(t=spokeThick) {
  difference () {
    cylinder(t, d=srvoCyl) ;
    translate([0,0, -antiZ]) // hole for spring wire cage axle
      cylinder(2*antiZ + t, d=springDia) ;
  }
}
axleShim(springDia) ;
translate([0,0, -3])
  axleShim(springDia) ;
translate([0,0, 3])
  axleShim(3) ;

module spoke () {
  difference () {
    union () {
      cube([spokeThick, spokeWd, pi0W], true) ;
      rotate([0, 90, 0])  // support for axle hole
        cylinder(spokeThick, d=srvoCyl, center=true) ;
    }
    translate([0,0, -antiZ]) // hole for spring wire cage axle
      rotate([0, 90, 0])
      cylinder(2*antiZ + spokeThick, d=springDia, center=true) ;
  }
}

// The servo push rod needs to hook into a receiver (servoPush) on the tilt cage
// as part of a parallelogram
// https://slideplayer.com/slide/12922735/
//  - cageAxle (spoke center)
//  - servoGearCenter (mounted below on tilt_frame)
//      translate([pinCylOD/2, -bodyHt - gearsHt - frameTopOD/2,
//          pinCylsSep + pinCylThick + frameTopThick - frameBaseThick])
//  - hole in servoArm (sg90srvoArm)
//  - center of servoCyl (placement calculated below)

// height of cage axle above tilt_frame base is >= the hypotenuse length
// (clearance for cage to rotate) of right triangle whose leg lengths are
//  - tCgCapDeep + tCgRailUsable/2
//  - cageHt/2 (pi0W/2 + slop + tCgRailThick)
deg = 15 ;	// TODO - derive via pi0W/2, tCgRailUsable, tCgCapDeep, capThick

// y-depth from axle hole to servo linkage hole
// z-height from axle hole to servo linkage hole
srvoCyl = 6 ;	// dia of srvo push attachment

// attachment for servo linkage to push against
module servoPush (x=0, y=0, z=0) {
  supportY = y + tCgRailUsable/2 + tCgCapDeep - spokeThick/2 ; // axleCtr to mid-Zsupport
  translate([x, y, z]) {
    rotate([0, 90, 0])
      cylinder(spokeThick, d=srvoCyl) ;
    translate([0, -supportY, -srvoCyl/2])
      cube([tCgRailThick, supportY, srvoCyl]) ;
  }
}

module servoPushDiff(x=0, y=0, z=0) {
  translate([x, y, z])
    rotate([0, 90, 0]) {
      cylinder(spokeThick, d=inch/32) ;
      translate([0,0, -antiZ])
        cylinder(antiZ + spokeThick * .45, d1 = srvoCyl, d2 = inch/32) ;
      translate([0,0, spokeThick * .55 ])
        cylinder(antiZ + spokeThick * .45, d1 = inch/32, d2 = srvoCyl) ;
  }
}

module llQ() { // lower-front quarter of rail
  legWS = tCgRailThick + pi0crnR + slop ;	// total width/height of rail
  Hdeep = tCgCapDeep + tCgRailUsable/2 + antiZ ;	// half of total depth

  translate([0, // -(tCgRailThick + slop + pi0L/2),
             antiZ - Hdeep,
             -(tCgRailThick + slop + pi0W/2)])
    difference() {
      union() { // start lower-left@XY=0
        // horizontal portion of L-rail
        cube([legWS, Hdeep, tCgRailThick]) ;
        // vertical portion of L-rail
        cube([tCgRailThick, Hdeep, legWS]) ;
        // Zsupport struts
        translate([spokeThick/2, spokeThick/2, 0])
          cylinder(antiZ + tCgRailThick + slop + pi0W/2, d=spokeThick) ;
      }
      // hole for frameCap
      translate([legWS - springDia, tCgCapDeep - springDia/2, -antiZ])
        cylinder(2*antiZ + tCgRailThick, d=springDia) ;
    }
}
// #llQ() ;

module llH() { // lower half of rail
  llQ() ;
  mirror([0, 1, 0]) // left-to-right across y=0 plane
    llQ() ;
}
// #llH() ;

module lRail() { // left rail
  translate([-(tCgRailThick + slop + pi0L/2), 0,0]) {
    difference() {
      union() {
        llH() ; // bottom half
        mirror([0,0, 1]) // bottom-to-top across z=0 plane
          llH() ; // mirrored to become top half
        translate([tCgRailThick/2, 0,0])
          spoke() ;
        servoPush(0, -sg90srvoArm * cos(deg), -sg90srvoArm * sin(deg));
      }
      servoPushDiff(0, -sg90srvoArm * cos(deg), -sg90srvoArm * sin(deg));
    }
  }
}
// #lRail() ;

module Rails() { // both rails
  rotate([180,0, 0])
    mirror([0,0, 180])
    lRail() ;
  mirror([1, 0,0])
    rotate([180, 0,0])
    lRail() ;
}

module cruft() {
            /* // support for shroud wire
            translate([0, tCgRailThick + springDia / 2, tCgRailThick - springDia / 2])
              rotate([0,90,0])
                // cylinder(legW, r = tCgRailThick + springDia / 2) ;
                %linear_extrude(legW)
                  intersection() {
                    sz = springDia + 2 * tCgRailThick ;
                    circle(d = sz) ;
                    translate([0,-sz/2,0])
                      square(sz, sz / 2) ;
                  }
             */
          /*
          // spring-wire dia hole thru support for shroud wire
          // translate([0, tCgRailThick - springDia / 2, tCgRailThick - springDia / 2])
          translate([0, springDia, springDia])
            rotate([0,90,0])
            translate([0,0, -antiZ])
            cylinder(2*antiZ + legW, d = springDia) ;
          translate([0, railDepth + spokeThick + railDepth - spokeThick,
                     springDia])
            rotate([0,90,0])
            translate([0,0, -antiZ])
            cylinder(2*antiZ + legW, d = springDia) ;
          */
}

module clip(xtra=0) {
  // cap protrusion to hook into frame
  translate([-pi0L/2, 0, pi0W/2])
  translate([pi0crnR - springDia,
             tCgCapDeep - springDia/2,
             -antiZ]) {
    cylinder(2*antiZ + tCgRailThick - (springDia + xtra)/2, d=springDia+xtra) ;
    translate([0,0, tCgRailThick - (springDia + xtra)/2])
      sphere(d=springDia+xtra) ;
  }
}

module capRect(x=pi0L, y=tCgCapDeep, z=pi0W, less=0) {
  antiY = (less==0) ? 0 : antiZ ;
  translate([0, antiY + tCgCapDeep/2, 0])
    translate([0, -antiY, 0])
    cube([x-less, 2*antiY + y, z-less], center=true);
}

// TODO - re-work to Q/H/Full ?
capFrameThick = .9 ;	// springDia //tCgRailThick) ; // pi0crnR - 1.5) ;
module frameCap() {
  translate([0, -tCgCapDeep/2, 0]) {
    difference() {
      capRect() ;
      capRect(less = 2 * capFrameThick) ;
    }
    clip(-slop) ;
    mirror([0,0,1])
      clip(-slop) ;
    mirror([1,0,0])
      clip(-slop) ;
    mirror([0,0,1])
      mirror([1,0,0])
      clip(-slop) ;
  }
}
module axle() {
  axleL = pi0L + 2*slop + tCgRailThick + 14 ;
  rotate([0, 90, 0])
    cylinder(axleL, d=springDia, center=true);
  // translate([axleL/2 - springDia/2, 0, -10])
    // cylinder(10, d=springDia);
}

module board() {
  rotate([90, 0,0])
    pi0shim() ;
}

module tilt_cage() {
  Rails() ;
  translate([0, -(tCgRailUsable + tCgCapDeep)/2, 0])
    frameCap() ;
  translate([0, (tCgRailUsable + tCgCapDeep)/2, 0])
    rotate([0,0,180])
  union() { // blend in a pi0shim with the rear frameCap
    frameCap() ;
    translate([0, -tCgCapDeep/2, 0])
    rotate([-90, 0,0])
      pi0shim(capFrameThick, tCgCapDeep) ;
  }

  %axle() ;
  translate([0, -3, 0])
    %board() ;
  translate([0, -8, 0])
    %board() ;
  translate([0, 3, 0])
    %board() ;
  translate([0, 12, 0])
    %board() ;
}
tilt_cage() ;

/*
translate([pi0L/2 + slop + tCgRailThick, 0,0])
rotate([0, 90, 0])
%circle( sg90srvoArm ) ;
*/

// http://integument.com/fluoropolymer-products/optically_clear_film/
// http://integument.com/fluoropolymer-products/films_sheets/
//  - By overlapping seam areas and applying pressure, a tight seal forms.
// https://www.professionalplastics.com/PTFE_FilmTeflon
//  - PTFE ... cannot be processed by melting => compressed and sintered
