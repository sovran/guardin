// © 2019,2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/thru_holes.scad

// gear to stator #4 screw holes to diff out from pieces
// 3/8, 5/8, 1" screws (stainless/brass phillips) hold gear to stator
// available Ace/HD/Lowes - any of wood/machine/sheet_metal

include <params.scad> ;

module thruHoles(h=0, zXlat=0, bite=true, sink=true, shorten1 = 0) {
  translate([0,0,zXlat]) {	// from the bottom (z=xlat)
    for (rot = [0 : 60 : 120]) {
      rotate(rot) 
        for (offset = [thruOffset, -thruOffset]) { // pair along x-axis
          translate([offset, 0, -antiZ]) {
            // hole
            cylinder(h - (rot==0 ? shorten1 : 0) + 2*antiZ, d=thruOD * (bite ? 1.01 : 1.15));
            if (sink) {
              translate([0,0, -antiZ])  // countersink the countersink
                cylinder(.5 + antiZ * 2, d=screw4HdDia * 1.1);
              translate([0,0,.5])  // countersink
                cylinder(screw4HdHt, d1=screw4HdDia, d2=screw4MjDia);
            }
          }
        }
      }
  }
}
