The a360 STL links below are being deprecated in favor of [OpenSCAD](https://www.openscad.org/) files.

[OpenSCAD](https://en.wikipedia.org/wiki/OpenSCAD) is a multi-platform tool for scripting 3D CAD ojects (and can trivially save [STL](https://en.wikipedia.org/wiki/STL_(file_format))s)

 * [Slip ring jig](../slip_ring/README.md)

 * [M2.5 rod](https://a360.co/2Ixevqc)
 * [Pi Zero PWM crib](https://a360.co/2wUx7it)
 * [Pi Zero camera shim](https://a360.co/2KGvAyH)
 * [Pi Zero HDMI shim](https://a360.co/2KEDmJj)
 * [Slip-ring cage floor](https://a360.co/2KGXeLF)
 * [Slip-ring brush cage (geared)](https://a360.co/2wV8AtI)
 * [Slip-ring core](https://a360.co/2wV8Zwe)
 * [Tilt cage](https://a360.co/2LgRh9y)
 * [Turret](https://a360.co/2PBtDGT)
 * [Turret servo gear](https://a360.co/2wZqWK8)
 * [Turret servo shim](https://a360.co/2KBqBzi)
 * [Turret servo spring holder](https://a360.co/2LgMbKj)
 * [wrench](https://a360.co/2KDNBxm)
