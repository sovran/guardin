// © 2019-2021 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/pi0shim.scad
//
// Create a pi0 shim with optional standoffs

include <params.scad> ;

pi0demiL = pi0L / 2 - pi0crnR ;  // ~.5 Length/long leg to mid-hole
pi0demiW = pi0W / 2 - pi0crnR ;  // ~.5 Width/short leg to mid-hole

module qto(w,t,s,s2,wwgt) { // create quarter shim // https://en.wikipedia.org/wiki/Quarto
  // in the +X / -Y quadrant
  translate([0, -pi0W/2, 0]) {
    // long leg (x)
    translate([-antiZ, 0,0])
      cube([pi0demiL + antiZ, w - wwgt, t]) ;
    // short leg (y)
    translate([pi0L/2 - w, pi0crnR, 0])
      cube([w, pi0demiW + antiZ, t]) ;
    // wrap legs around standoff
    translate([pi0demiL, pi0crnR, 0])
      intersection() {
        cylinder(t, r=pi0crnR) ;
        translate([0,0, -antiZ])
          rotate(-90)
            cube([pi0crnR + antiZ, pi0crnR + antiZ, 2*antiZ + t]) ;
      }
    // standoff(s)
    translate([pi0L/2-pi0mntCtrXY, pi0mntCtrXY, -s2])
      cylinder(s + t + s2, d=pi0mntPadD);
  }
}

// copy object and mirror across vector
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Tips_and_Tricks#Create_a_mirrored_object_while_retaining_the_original
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/User-Defined_Functions_and_Modules#children
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Transformations#mirror

module mirror_copy(v = [1, 0, 0]) { // default mirror plane = yz (normal:x=1)
  children() ;
  mirror(v) children() ;
}

module pi0shimBlank(wide=2, thick=1, standoffHt=0, stand2=0, wWGtLW=0) {
  mirror_copy([0, 1, 0]) // mirror/copy half-shim across y-axis
    mirror_copy() // mirror/copy quarter-shim across (default) x-axis
      qto(wide, thick, standoffHt, stand2, wWGtLW) ;
}

module pi0shimMntHoles(thick=1, standoffHt=0, stand2=0) {
  mirror_copy([0, 1, 0])
    mirror_copy()
      translate([pi0L/2 - pi0mntCtrXY, pi0mntCtrXY - pi0W/2, -antiZ - stand2])
        cylinder(2*antiZ + standoffHt + thick + stand2,
		 d = pi0mntHoleD + 2.6*slop) ;
}

module pi0shim(wide=2, thick=1, standoffHt=0, stand2=0, wWGtLW=0) {
  difference() {
    pi0shimBlank(wide, thick, standoffHt, stand2, wWGtLW) ;
    pi0shimMntHoles(thick, standoffHt, stand2) ;
  }
}

pi0shim(1.25, .75, 4.75 - .75) ;
