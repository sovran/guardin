// © 2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/tilt_frame.scad

include <params.scad> ;
include <sg90_params.scad> ;
use <sprinkler.scad> ;
use <sg90.scad>
use <tilt_cage.scad>

supportThick = 4.5 ;
frameTopThick = 2.75 ;

armThick = 2.5 ;

frameBaseDeep = 33.6 ;
frameBaseThick = 5 ;
frameTopOD = 18.1 ;	// => _params
// frameBaseLen = (pi0L + 2 * (1.5 + slop + tCgRailThick - armThick) - frameTopOD) / 2 ;
frameBaseLen = (pi0L + 2*(slop + tCgRailThick -armThick) - frameTopOD)/2 + 3 ;
;
framePinOD = 4.85 ;	// => _params

axleHt = tCgAxleHt + frameBaseThick ;
axleHubDia = 6 ;
echo("axleHt = ", tCgAxleHt) ;

lowerPinHoleDepth = 4.2 ;
pinBase2screw = 10 ;	// z above bottom pin hole to bottom of screw
pinCtr2nose = 13.1 ;	// x-dist from pin ctr to brow nose-over
pinCylOD = 10.2 ;	// => _params
pinCylThick = 6.3 - frameTopThick ;	// cut two 10.5mm(6.3 + 4.2) sections from pin
pinCylsSep = 25 ;
botPinCylRise = 2 ;
topPinCylHt = 3.1 ;
ribInset = 3 ;

module body () {
  translate([0,0, -lowerPinHoleDepth]) {
      translate([-h2oTubeCtrX, -h2oTubeCtrY, -(27.5 - lowerPinHoleDepth)]) {
        sprinkler() ;
        import_scan() ;
        // scan_top() ;
      }
  }
}

sg90bodyWd = 12.37 ;
wingWd = 12 ;	// FIXME - dup ?

module sg90mount() {
  translate([pinCylOD/2, -frameTopOD/2 - bodyHt + wingOffsetZ + wingThick,
             pinCylsSep + pinCylThick + frameTopThick - frameBaseThick - sg90bodyWd])
  difference() {
    // cube([wingSpan, wingScrewLen - wingThick, 1 + wingWd]);
    cube([wingSpan, bodyMidHt, 1 + wingWd]);
    translate([wingSpan/2, -antiZ, wingWd/2 ])
      rotate([-90, 0, 0])
      cylinder(wingScrewLen - wingThick, d = wingScrewOD) ;
  }
}
translate([bodyLen + wingSpan, 0,0])
  sg90mount() ;
sg90mount() ;

module arm() {
  rotate([90, 0,0])
    difference() {
      linear_extrude(armThick) {
        polygon([[0,0],
                 [frameBaseDeep/2 - axleHubDia/2, axleHt],
                 [frameBaseDeep/2 + axleHubDia/2, axleHt],
                 [frameBaseDeep, 0]]);
        translate([frameBaseDeep/2, axleHt, 0])
          circle(d=axleHubDia);
      }
      translate([frameBaseDeep/2, axleHt, -antiZ])
        cylinder(2*antiZ + armThick, d=springDia);
    }
}

module axle_clip() { // a pair of nibs forming an axle clip @x,z=0
  nibHt = 4 ;
  nibWd = 2.5 ;
  nibDeep = springDia * 2.5 ;
  squeeze = .25 ;

  translate([-nibWd - squeeze - springDia/2, antiZ - nibDeep, 0]) {
    cube([nibWd, 2*antiZ + nibDeep, nibHt]) ;
    translate([nibWd + springDia/2, 3*springDia/2, 4.5])
      %cylinder(15, d=springDia, center=true);
    translate([nibWd - squeeze + springDia, 0,0])
      cube([nibWd, 2*antiZ + nibDeep, nibHt]) ;
  }
}

module left_frame() { // Y<0
  // base of left frame/arm
  translate([-pinCtr2nose,
             -(frameBaseLen + frameTopOD/2),
             31.2 - frameBaseThick])
    cube([frameBaseDeep + 17, frameBaseLen, frameBaseThick]) ; // FIX

  // base/lip under frameTop (for stability on Y-rot)
  translate([-pinCtr2nose + 1,
             -antiZ - frameTopOD/2,
             31.2 - frameBaseThick])
    cube([frameBaseDeep - 5,
          (frameTopOD - pinCylOD)/2 - 2,
          pinCylThick -2]) ; // FIX

  // attachment/support
  translate([pinCylOD/2, 0, pinCylsSep/2])
    rotate([90, 0, -90])
    linear_extrude(supportThick) {
      polygon([[0,0],
               [0, pinCylsSep/2],
               [pinCylOD/2, pinCylsSep/2],
               [frameTopOD/2, pinCylsSep/2 + 6.3 - frameBaseThick + antiZ],
               [frameTopOD/2 + frameBaseLen,
                pinCylsSep/2 + 6.3 - frameBaseThick + antiZ]]) ;
  }
  translate([-pinCtr2nose,
             antiZ - frameBaseLen - frameTopOD/2,
             pinCylsSep + pinCylThick + frameTopThick - frameBaseThick])
    arm() ;
  // clip to secure tilt_cage axle
  translate([-pinCtr2nose + frameBaseDeep/2,
             -frameTopOD/2 - frameBaseLen - armThick,
             pinCylsSep + pinCylThick + frameTopThick
              - frameBaseThick + axleHt - 12])
    axle_clip() ;
}

module right_frame() { // Y>0
  difference() {
    union() {
      // base of right frame/arm
      translate([-pinCtr2nose, -framePinOD/2, 31.2 - frameBaseThick])
        cube([frameBaseDeep, pinCylOD/2 + frameTopOD/2 + frameBaseLen,
              frameBaseThick]) ;

      // attachment/support
      translate([pinCylOD/2 - supportThick, 0, pinCylsSep/2])
        rotate([90, 0, 90])
        linear_extrude(supportThick) {
          polygon([[0,0],
                   [0, pinCylsSep/2],
                   [pinCylOD/2, pinCylsSep/2],
                   [frameTopOD/2, pinCylsSep/2 + 6.3 - frameBaseThick + antiZ],
                   [framePinOD/2 + frameTopOD/2 + frameBaseLen,
                    pinCylsSep/2 + 6.3 - frameBaseThick + antiZ]]) ;
      }
    }

    // cut away from top frame layer
    translate([0,0, pinCylsSep + pinCylThick]) {
      cylinder(frameTopThick, d=frameTopOD);
      translate([0, -frameTopOD/2, -antiZ])
        cube([frameTopOD/2, frameTopOD/2, 2*antiZ + frameTopThick]) ;
      translate([-antiZ - pinCtr2nose, -frameTopOD/2, 0])
        cube([2*antiZ + pinCtr2nose, frameTopOD, frameTopThick]) ;
    }

    // cut away from lower frame
    translate([0,0, -antiZ + pinCylsSep]) {
      cylinder(2*antiZ + pinCylThick, d=pinCylOD);
      translate([0, -pinCylOD/2, -antiZ])
        cube([pinCylOD/2, pinCylOD/2, 2*antiZ + pinCylThick]) ;
      translate([-antiZ - pinCtr2nose, -pinCylOD/2, 0])
        cube([2*antiZ + pinCtr2nose, pinCylOD, 2*antiZ + pinCylThick]) ;
      translate([-pinCtr2nose, 0,0])
        cube([2.1, frameTopOD/2, 2*antiZ + pinCylThick]) ;
    }
  }
  translate([-pinCtr2nose,
             -antiZ + framePinOD/2 + frameTopOD/2 + frameBaseLen + armThick,
             pinCylsSep + pinCylThick + frameTopThick - frameBaseThick])
    arm() ;
}

module struct() {
  difference() {
    union() {
      cylinder(pinCylsSep, d=pinCylOD) ; // pinCylOD, frameTopOD /\ ?
      left_frame() ;
      right_frame() ;
    }
    cylinder(pinCylThick + frameTopThick, d=framePinOD + slop) ;
    translate([0,0, pinCylsSep -(pinCylThick + frameTopThick)])
      cylinder(antiZ + pinCylThick + frameTopThick, d=framePinOD + slop) ;

    // hole for #8 pan head sheet metal screw - 3/8 or 1/2 "long , e.g. :
    // https://www.homedepot.com/p/Everbilt-8-x-3-8-in-Phillips-Pan-Head-Stainless-Steel-Sheet-Metal-Screw-6-Pack-801021/204275386
    translate([0, -frameTopOD/2, pinCylsSep/2])
      rotate([-90, 0, 0])
      cylinder(frameTopOD, d=screw8MjDia - slop) ;
    // pan head indent
    translate([0, -pinCylOD/2 + screw8HdHt/2, pinCylsSep/2])
      rotate([90, 0, 0])
      cylinder(screw8HdHt/2 + 8, d=slop+screw8HdDia) ;

    // slice pinCyl along XY plane
    translate([-frameTopOD/2 - antiZ, -antiZ, -antiZ])
      cube([frameTopOD + 2 * antiZ, 2 * antiZ, 2 * antiZ + pinCylsSep]) ;
    }
}

%body() ;
struct() ;

translate([pinCylOD/2, -bodyHt - gearsHt - frameTopOD/2,
           pinCylsSep + pinCylThick + frameTopThick - frameBaseThick])
  rotate([-90, 0,0])
  %sg90() ;

translate([pinCylOD/2 - springDia, 0,
           pinCylsSep + pinCylThick + frameTopThick -frameBaseThick + axleHt])
  rotate([0, -45, 0])
    rotate([0,0, -90])
    %tilt_cage() ;
