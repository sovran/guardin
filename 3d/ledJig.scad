// © 2021 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/rPicoShim.scad

// https://en.wikipedia.org/wiki/Perfboard
// e.g. https://www.amazon.com/gp/product/B072Q1H6GX , or 1/5 that @Ali
pbThick = 1.25 ;

$fn = 100 ;
antiZ = .001 ;

// https://en.wikipedia.org/wiki/Pin_header
// 2.54 mm (0.100 in) with 0.64 mm (0.025 in) square pins[6]
//   or precision machined 0.50 mm (0.020 in) round pins[7]
pinW = .64 ;
pinDia = pinW * sqrt(2) ; // .905
padW = 2.54 ;

xL = 20 ; // 2x20 pin header
yW = 2 ;

ledBaseW = 6 ; // LED dome diameter at base

module pinCutOut() {
  translate([ledBaseW/2 - pinDia/2, -(padW + pinDia)/2, -antiZ])
    cube([ledBaseW/2 + pinDia, padW + pinDia, pbThick + 2*antiZ]) ;
}

module 1side() {
  // 15 pins x (pin-to-pin, 17 edge-to-edge), 7 pins y (topPin-tobottomPin)
  // cube([15*padW, 7*padW, pbThick], center=true ) ;
  translate([32/2, 0, 0])
  difference() {
    translate([0, -22/2, 0])
      cube([ledBaseW, 22, pbThick]) ; // , center=true ) ;
    pinCutOut() ;
    translate([0, 3*padW, 0])
      pinCutOut() ;
    translate([0, -3*padW, 0])
      pinCutOut() ;
  }
}
1side() ;
mirror([1,0,0])
  1side() ;
translate([0,0, .1])
cube([33, 22, .2], center=true) ;


module 1pinShim() {
  translate([padW/2, padW/2, picoThick/2])
    difference() {
      cube([padW + antiZ, padW + antiZ, picoThick], center=true) ;
      cylinder(picoThick + 2*antiZ, d=pinDia + .75, center=true) ;
    }
}

module pinShim() {
  for(x=[0:xL-1])
    for(y=[0:yW-1])
      translate([x*padW, y*padW, 0])
         1pinShim() ;
}
// %pinShim() ;
