// © 2020 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/blob/master/3d/pwm_shim.scad
//
// Create a pwm ctl board shim/sabot
// https://www.adafruit.com/product/815
// https://learn.adafruit.com/16-channel-pwm-servo-driver/downloads
// clones e.g.:
// https://www.amazon.com/SunFounder-PCA9685-Channel-Arduino-Raspberry/dp/B014KTSMLA
// https://usa.banggood.com/PCA9685-16-Channel-12-bit-PWM-Servo-Motor-Driver-I2C-Module-p-1170343.html
// https://www.aliexpress.com/item/4000468996665.html ...
// https://www.youtube.com/watch?v=MB9DGwyGguw
// https://en.wikipedia.org/wiki/Sabot_(firearms)

include <params.scad>
use <pi0shim.scad> ;

module quarter(wide, thick, dia=2.6, groWd=0) { // create 1/4
  cornerRadius = pwmBrdCornerRadius ;
  lenLessRadius = pwmBrdWd/2 - cornerRadius ;
  deepLessRadius = pwmBrdDeep/2 - cornerRadius ;

  difference() {
    union() {
      translate([0, deepLessRadius, 0])
        translate([-antiZ, cornerRadius - wide, 0]) // X-leg
        cube([antiZ + lenLessRadius, groWd + wide, thick]) ;
      translate([pwmBrdWd/2 - cornerRadius, 0,0]) {
        translate([cornerRadius - wide, -antiZ, 0])	// Y-leg
          cube([wide + groWd, antiZ + deepLessRadius, thick]) ;
        translate([0, deepLessRadius, 0])
          cylinder(thick, r = groWd + cornerRadius) ;
      }
    }
    translate([lenLessRadius, deepLessRadius, -antiZ])
      cylinder(2*antiZ + thick, d = dia) ;
  }
}

module pwmShim(wide=2, thick=1, dia=2.6, groWd=0) {
  quarter(wide, thick, dia, groWd) ;
  mirror([1,0,0])
    quarter(wide, thick, dia, groWd) ;
  mirror([0, 1, 0]) {
    quarter(wide, thick, dia, groWd) ;
    mirror([1,0,0])
      quarter(wide, thick, dia, groWd) ;
  }
}

channelDeep = 1.5 ;
pi0shimThick = 3.2 ;
pwmBrdThick = 1.3 ;
cutThick = .4 + pwmBrdThick ;
nibHt = .25 ;
nibCut = cutThick - 2*nibHt ;

capOD = 8.2 ;
pwrBlockWd = 7.7 ;
axleGuard = 2 ;

degs = 62.85 ; // TODO - parameterize	// 27.15
lessL = pi0L/2 - pi0W / 2 /  tan(degs - 90) ; // * sin(degs) ;
module hsplit() {
  translate([-pi0L/2 - antiZ, -pi0W/2 - antiZ, -channelDeep/2]) {
    linear_extrude(antiZ)
      polygon([[0,0],
               [0, pi0W + 2*antiZ],
               [pi0L - lessL, pi0W + 2*antiZ],
               [lessL  + 2*antiZ, 0]]);
  }
}

module split() {
  hsplit() ;
  mirror([1,0,0])
    mirror([0,0,1])
    mirror([0,1,0])
    hsplit() ;
  rotate([0,0, degs]) // vertical split
    cube([antiZ, norm([pi0L + pi0W]), channelDeep + 2*antiZ], center=true);
}

difference() {
  translate([0,0, -(axleGuard + pi0shimThick/2)]) // + cutThick/2)])
  difference() { // tuck pwmBrd into pi0 shim
    // (wide=2, thick=1, standoffHt=0, stand2=0, wWGtLW=0)
    union() {
      pi0shim(wide = channelDeep + (pi0L - pwmBrdWd)/2,
              thick = pi0shimThick + axleGuard,
              standoffHt = 4, stand2 = 6.8, wWGtLW = .5) ;
      translate([0,0, pi0shimThick + axleGuard -antiZ])
        pi0shim(wide = channelDeep + (pi0L - pwmBrdWd)/2, thick = 1) ;
    }
    translate([0,0, axleGuard + (pi0shimThick - cutThick)/2])
      pwmShim(5, cutThick, 1.5, .8) ; // carve out pwmBrd channel
  }
  split() ;
}

/*
translate([0,0, 8])
  #pwmShim(.5, .05, 1, 5) ;
*/
