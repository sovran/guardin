// © 2019 Jim Freeman.  All rights reserved.
// https://creativecommons.org/licenses/by-sa/2.0/
// https://gitlab.com/sovran/guardin/tree/master/3d/turret_sg90_shims.scad

// https://a360.co/2LgMbKj	- old a360 piece

// turret servo e.g. :
// http://www.towerpro.com.tw/product/sg90-7/
// http://www.feetechrc.com/product/analog-servo/micro-1-3kg-cm-360-degree-continuous-rotation-servo-fs90r/

// 5/16 x 1-1/2 x .020 WG Compression Spring
// https://www.amazon.com/1-1-020-Compression-Spring-pieces/dp/B00L1IIXJU

use <sg90.scad>
include <sg90_params.scad>

inch = 25.4 ;
antiZ = .001 ;

shimWd = bodyWdMin ;
shimHt = bodyHt - bodyCapHt - wingOffsetZ - wingThick ;

shimThick = wingSpan ;

springDia = 5 / 16 * inch ;

screwOD = 2 - .1 ;
screwLen = 8 ;

module springShim() {
  difference () {
    cube([shimWd, shimHt, shimThick]) ;
    // drill for springDia+
    translate([shimWd / 2, shimHt / 2, -antiZ]) {
      // spring holder/cavity
      cylinder(shimThick + antiZ * 2, d = springDia + .15) ;
      // chamfer - smooth spring compressing into cavity
      cylinder(1, d1 = springDia + 1.15, d2 = springDia) ;
    }
    // drill for mount screw
    translate([shimWd / 2, -antiZ, shimThick / 2])
      rotate([-90, 0,0])
        cylinder(shimHt / 2 + antiZ * 2, d = screwOD) ;
  }
}

module underWing() {
  difference() {
    union() {
      // underneath the wing
      cube([wingSpan, shimWd, gearsHt + wingOffsetZ]);
      // underneath the servo to center of big gear (-unsharpen)
      cube([wingSpan + bodyWdMax / 2 - 2, shimWd, gearsHt]) ;
      translate([-5.5 + antiZ, 0,0])
        cube([5.5, shimWd, gearsHt]) ;
    }
    // screw hole
    translate([wingSpan-wingHoleOff, bodyWdMin/2,
               gearsHt + wingOffsetZ - screwLen])
      cylinder(screwLen + antiZ, screwOD) ;
    translate([wingSpan, 0, -antiZ]) {
      translate([bodyWdMax / 2, bodyWdMax / 2,0])
        // large gear only
        cylinder(gearsHt + antiZ * 2, d = bodyWdMax + .15) ;
     }
  }
}

module gearShim() {
  difference() {
    union() {
      // underneath the wing
      cube([wingSpan, shimWd, gearsHt + wingOffsetZ]);
      // underneath the servo to center of big gear (-unsharpen)
      cube([wingSpan + bodyLen - bodyWdMax / 2 - 2, shimWd, gearsHt]) ;
    }
    // screw hole
    translate([wingSpan-wingHoleOff, bodyWdMin/2,
               gearsHt + wingOffsetZ - screwLen])
      cylinder(screwLen + antiZ, screwOD) ;
    // gear cutouts
    translate([wingSpan, 0, -antiZ]) {
      translate([bodyLen - bodyWdMax / 2, bodyWdMax / 2,0]) {
        // large gear first
        cylinder(gearsHt + antiZ * 2, d = bodyWdMax + .15) ;
        translate([-bodyWdMax / 2, 0,0])
          cylinder(gearsHt + antiZ * 2, d = smGearOD + .2) ;
        }
     }
  }
}

module curtain() {
  deg = 10 ;
  ht = 8 ; // hypotenuse
  wd = .4 ;
  panes = 7 ;
  mountThick = .7 ;

  difference() {
    translate([wingSpan + bodyLen, 0, gearsHt + wingOffsetZ])
      rotate([-90, 0,0])
        linear_extrude(bodyWdMin) {
          square([wingSpan, mountThick]) ;
          for ( pane = [0 : 1 : panes-1] ) {
            translate([wingSpan - wd + (pane + (pane%2?1:0))* ht * sin(deg), 0])
              rotate(pane%2 ? deg : -deg)
                square([wd, ht]) ;
          }
        }
    // drill for mount screw
    translate([wingSpan + bodyLen + wingSpan/2,
              bodyWdMax/2,
              gearsHt + wingOffsetZ - mountThick - antiZ])
      cylinder(mountThick + 2*antiZ, d = screwOD) ;
  }
}
%curtain() ; // show actual placement
rotate([90, 0,0])
  curtain() ; // place for printing

translate([bodyLen + wingSpan * 2, bodyWdMax,0])
rotate([0,0, 180])
%sg90() ;

translate([0, (bodyWdMax - bodyWdMin)/2, 0]) {
  underWing() ;
  translate([bodyLen + wingSpan * 2, shimWd, gearsHt + wingOffsetZ + wingThick])
    rotate([90, 0, -90])
      springShim() ;
}

translate([bodyLen + 2*wingSpan, bodyWdMax, 0])
rotate([0,0, 180])
  %gearShim() ;
