// FIXME
function vsum(list, c = 0) = 
 c < len(list) - 1 ? 
 list[c] + vsum(list, c + 1) 
 :
 list[c];
