#!/usr/bin/python
# https://github.com/sovrajn/guarden/blob/master/cherubim.py
# Copyright (c) 2018 by Jim Freeman.  All rights reserved.
# This is Sovereign Software, part of a Sovereign System.
# https://creativecommons.org/licenses/by-sa/4.0/legalcode
#
# Typically invoked by a motion event triggering power-up of the rPi0 device
# running this script.  Turret will pan, seeking to recognize a target - if
# detected, invoke/assess/log deterrent measures, phone home (if possible),
# and exit (should result in powerdown).

# https://learn.adafruit.com/16-channel-pwm-servo-driver/library-reference
# https://cdn-learn.adafruit.com/downloads/pdf/adafruit-16-channel-servo-driver-with-raspberry-pi.pdf
import Adafruit_PCA9685 # PWM controller

import cv2  # https://docs.opencv.org/2.4/index.html
import math # https://docs.python.org/2/library/math.html
import os # https://docs.python.org/2/library/os.html
import signal # https://docs.python.org/2/library/signal.html
import sys # https://docs.python.org/2/library/sys.html
import time # https://docs.python.org/2/library/time.html

from random import randint # https://docs.python.org/2/library/random.html

# https://picamera.readthedocs.io/en/release-1.13/api_camera.html
from picamera import PiCamera
from picamera.array import PiRGBArray   # array from an RGB capture

class Timer:
  def __init__(self):
    self.start = float("%.4f" % time.time()) # fractional seconds since epoch
  def since(self):
    return float("%.4f" % (time.time()- self.start))
  def str(self, t=None):
    return time.strftime("%Y%b%d-%H:%M:%SGMT", time.gmtime(t))

# 0 degrees ~140   90 degrees ~235    180 degrees ~410
# (x=0, y=0) is the upper-left of an OpenCV image (y increases downward)
# (0,0) ----> X-axis   |
#                      V  Y-axis
class TiltServo:
  def __init__(self, ctr):  # = 0):
    self.chan = 1
    self.midY = ctr
    # self.pwm = {"incr":10, "down":435, "lvl":220, "up":140}
    self.pwm = {"incr":10, "down":440, "lvl":240, "up":140}
    self.pwm["cur"] = self.pwm["lvl"]
    pwm.set_pwm(self.chan, 0, self.pwm["cur"])
  def tilt(self, toY = None):
    if toY:
      mvBy = int(toY - self.midY)
      if mvBy < 0: # tilt up
        if self.pwm["cur"] > self.pwm["up"]:
          self.pwm["cur"] -= self.pwm["incr"]
      if mvBy > 0: # tilt down
        if self.pwm["cur"] < self.pwm["down"]:
          self.pwm["cur"] += self.pwm["incr"]
    else: # (lost/)no target - (re-)center tilt cage
      self.pwm["cur"] = self.pwm["lvl"]
    pwm.set_pwm(self.chan, 0, self.pwm["cur"])
    # time.sleep(0.5)
  def report(self):
    return self.report
  def stow(self):
    pwm.set_pwm(self.chan, 0, self.pwm["down"])
    time.sleep(0.2)
    pwm.set_pwm(self.chan, 0, 0)

# turret motor eg; https://www.google.com/search?q=fs90r+fitech+OR+feetech
# http://www.feetechrc.com/product/analog-servo/micro-1-3kg-cm-360-degree-continuous-rotation-servo-fs90r/
class PanServo:
  '''Continuous rotation servo ccw = 328, 343 | still = 358 | cw = 388 , 373'''
  # Of the params (time, speed) we could control, for now we'll
  # just use a set slow speed (use PID?) # spin = {'ccw':1, 'cw':-1}
  def __init__(self, ctr):  # = 0):
    self.chan = 0
    self.ctr = ctr
    self.dfltSpin = (1, -1)[randint(0,9) % 2 == 0] # default random spin ccw/cw
    self.still = 358 # center of this servo's deadzone
    self.move = 27 # +/- edge-ish of deadzone
    self.move = 37 # +/- edge-ish of deadzone

    self.start = time.time()
  def pan(self, toX = None):
    if toX: # turn left(+/ccw)/right(-/cw) towards target
      mvBy = int(toX - self.ctr)
      if (math.fabs(mvBy) / self.ctr) > 0.025: # if not close enough
        move = int(math.copysign(self.move, mvBy))
        pwm.set_pwm(self.chan, 0, move + self.still)
        time.sleep(0.2)
    else: # no target to turn to, so just turn in orig random spin
      move = self.dfltSpin * self.move
      pwm.set_pwm(self.chan, 0, move + self.still)
      time.sleep(0.6)
    pwm.set_pwm(self.chan, 0, self.still)
  def report(self):
    return self.report
  def stow(self):
    pwm.set_pwm(self.chan, 0, 0)

class Turret:
  '''pan(X)/tilt(Y) to center/track target'''
  def __init__(self, ctr):  # viewport center @ (X,Y)
    self.ctrX, self.ctrY = ctr
    self.tilt = TiltServo(self.ctrY)
    self.pan = PanServo(self.ctrX)
  def aim(self, toXY = None):
    self.pan.pan( toXY[0] if toXY else None)
    self.tilt.tilt(toXY[1] if toXY else None)
  def stow(self):
    self.tilt.stow()
    self.pan.stow()

class Counts:
  '''Count groups of acquisitions/losses of target(s)'''
  def __init__(self):
    self.epochs = [] # [ [time, count or []], ...]
    self.events = None
    self.mark = t.since()
  def event(self, target = None):
    if target:
      if type(self.events) is list:
        self.events.append( [ t.since(), target ] )
      else: # target newly acquired - close epoch w/record of non-events count
        self.epochs.append( [ self.mark, self.events ] )
        self.events = [target]
        print "target acquired @", t.since()
        deter.deploy()  # deploy countermeasures
    else:
      if type(self.events) is int: # still no target
        self.events += 1
      elif type(self.events) is list: # target is newly lost
        self.epochs.append([self.mark, self.events])
        self.mark = t.since()
        self.events = 0
        print "lost target @", t.since()
        deter.desist()
      else: # opening event is a dud
        self.events = 1
        self.mark = t.since()
        print "no target @", self.mark
  def non(self):
    return 0 if type(self.events) is list else self.events
  def done(self):
    self.epochs.append([self.mark, self.events])
    print self.epochs

class Deterrents:
  '''Deploy random/combined countermeasures eg; light, sound, motion, sprinkler, ...'''
  def __init__(self):
    # initialize speaker, lasers, LEDs, servo(s) for ..., ...
    pass
  def deploy(self):
    print "invoke deterrents"
  def desist(self):
    print "revoke deterrents - target lost"

def done():
  turret.stow()
  vid.release()
  try:
    os.remove('vid.avi')
  except OSError:
    pass
  os.symlink(avi, 'vid.avi')
  cam.close()
  events.done()
  print 'Done @', t.since()
  sys.exit(0)

# https://elinux.org/Rpi_Camera_Module#Technical_Parameters_.28v.2_board.29
# 3280 x 2464 pixel static images , video at 1080p30, 720p60 and 640x480p90
# Angle of View: 62.2 x 48.8 degrees
fps = 2 # lo-speed
vidmode = (640, 480)  # don't need/want hi-res (1280,720) 'HD' '720p'
blue = (255, 0, 0) ; green = (0, 255, 0) ; red = (0, 0, 255)    # BGR

# for now, just stare back : opencv-data::haarcascade_eye.xml
haarXML = './haar.xml'  # link to trained haar cascade for target recognition

t = Timer()
t0 = t.str()
print "Commencing @ ", t0
avi = 'AVIs/' + t0 + '.avi' # video file

# https://www.raspberrypi.org/documentation/hardware/camera/ [ v2 ]
# Horizontal field of view  62.2 degrees
# Vertical field of view    48.8 degrees
cam = PiCamera()    # start_preview ?  annotate_text ?
cam.rotation = 270
cam.framerate = fps
cam.resolution = vidmode  # don't need/want hi-res
cam.color_effects = (128,128)    # black/white w/infrared(Bayer ?)

raw = PiRGBArray(cam, size=vidmode)
vid = cv2.VideoWriter(avi, cv2.cv.CV_FOURCC(*'MJPG'), fps, vidmode)
cascade = cv2.CascadeClassifier(haarXML)

pwm = Adafruit_PCA9685.PCA9685()
pwm.set_pwm_freq(60)

turret = Turret([vidmode[0]/2, vidmode[1]/2]) # (X,Y) center of camera view
events = Counts()
deter = Deterrents()

print "Capturing @", t.since()
# camera draws 250 mA when active
for frame in cam.capture_continuous(raw, format="bgr", use_video_port=True):
  img = frame.array   # image as raw numpy array
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

  maxTgt = None
  maxTgtSize = 0
  # possibilities - 1st target ?  largest(/closest)?
  for (x, y, w, h) in cascade.detectMultiScale(gray, 1.3, 5):
    # (x,y) is lower-left corner of target rectangle; (w, h) is width/height
    sz = w * h
    if (sz > maxTgtSize ):
      maxTgt = (x, y, w, h)
      maxTgtSize = sz
    #https://docs.opencv.org/2.4/modules/core/doc/drawing_functions.html
    cv2.rectangle(img, (x,y), (x+w, y+h), green, 2) # frame all targets in green
    cv2.putText(img, str(sz)+"px", (x+3,y+12), cv2.FONT_HERSHEY_PLAIN, 1,  green, 1, 8, False);

  atTgt = None
  if maxTgt : # deal with largest(/nearest?) recognized target
    (x, y, w, h) = maxTgt
    cv2.rectangle(img, (x,y), (x+w, y+h), red, 2) # re-frame primary in red
    atTgt = [x + int(w/2), y + int(h/2)]  # viewport center to target

  vid.write(img)  # append frame to video
  events.event(maxTgt)   # record frame event
  turret.aim(atTgt)  # seek for or center on target

  if events.non() > 10: # several contiguous frames find no target available
    break
  if t.since() > 180.0: # cap the total time
    break
  raw.truncate(0) # clear stream to prep for next frame # raw.seek(0) # ?

done()

def sigint_handler(signal, frame):
  signal.signal(signal.SIGINT, done)
